PYTHONVER = $(shell python -c 'import sys; print("%d.%d" % (sys.version_info.major, sys.version_info.minor))')
F90FLAGS = -fopenmp -O3 -march=native -Wa,-q
LIBS = -lgomp

# F90FLAGS = -Ofast -march=native -Wa,-q
# LIBS = 

all:	_glsausage.so

%.so:	%.f90
	f2py-$(PYTHONVER) -DF2PY_REPORT_ON_ARRAY_COPY -c $< -m _glsausage --f90flags='$(F90FLAGS)' $(LIBS)

clean:
	rm _glsausage.so
