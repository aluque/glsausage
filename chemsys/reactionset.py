import logging
logging.basicConfig(format='[%(asctime)s]  %(message)s', level=logging.DEBUG)

from numpy import *
from scipy import sparse
import re

from IPython import embed

class Reaction(object):
    def __init__(self, lhs, rhs, rate):
        self.lhs = lhs
        self.rhs = rhs
        self.rate = rate
        
    def rate(self, status):
        raise NotImplementedError("You must specify the reaction rate")
    
    def __str__(self):
        def multi(n, s):
            if n == 1:
                return s
            else:
                return "%d * %s" % (n, s)
        
        def joinside(side):
            return " + ".join(multi(n, s) for n, s in side)

        signature = "%s -> %s" % (joinside(self.lhs), joinside(self.rhs))

        return signature


class ReactionSet(object):
    def __init__(self):
        self.species = dict()
        self.reactions = []
        self.nspecies = 0
        self.nreactions = 0
        
    def add(self, signature, rate):
        lhs, rhs = parse(signature)
        for n, s in lhs + rhs:
            if not s in self.species:
                self.species[s] = self.nspecies
                self.nspecies += 1
                
        self.reactions.append(Reaction(lhs, rhs, rate))
        self.nreactions += 1


    def initialize(self):
        """ Builds the inner structures needed for running the model. """
        # R[i, j] gives the contribution to dn[i]/dt of a reaction j
        R = zeros((self.nspecies, self.nreactions))

        # For a reaction j, F[j] is an array of species indices i1, i2,...
        # such that the production of j is k * dens[i1] * dens[i2] ...
        self.F = []

        for j, r in enumerate(self.reactions):
            for f, s in r.rhs:
                i = self.species[s]
                R[i, j] += f

            l = []
            for f, s in r.lhs:
                i = self.species[s]
                l.extend([i] * f)
                R[i, j] -= f

            self.F.append(array(l))

        self.R = sparse.csr_matrix(R)
        
        logging.info("Model compiled")


    def derivs(self, n, EN, T):
        """ Calculates the dn/dt for each species from the densitites
        n and the status:
        
        n:      Array with shape n[nspecies, m]
        EN, T:  Array with shape (m)
        """

        
        # First we calculate all reaction rates [rates ~ (m, nreactions)].
        rates = c_[list(r.rate(EN, T) for r in self.reactions)]

        # We calculate only in as many point as the length of EN
        m = len(EN)

        # Now the density products
        nprod = array([prod(n[self.F[j], :m], axis=0)
                      for j in range(self.nreactions)])

        
        # Then we are set.
        return self.R.dot(nprod * rates)

    
    def zero_densities(self, m):
        return zeros((self.nspecies, m))

    def set_species(self, n, s, newval):
        n[self.species[s]] = newval

    def get_species(self, n, s):
        return n[self.species[s]]
    
    def print_species(self, n):
        for s, i in self.species.items():
            print('%6s' % s, n[i, :])

    def print_summary(self):
        print("SPECIES:\n %s" % ", ".join(self.species))
        print("REACTIONS:\n  %s" 
              % "\n  ".join(str(x) for x in self.reactions))  


RE_ARROW = re.compile(r"\s*[-=]+>\s*")
RE_PLUS = re.compile(r"\s+\+\s+")
RE_MULTIPLICITY = re.compile(r"((\d+)\s*\*\s*)?([\w+-]+)")

def parse(signature):
    lhs, rhs = (RE_PLUS.split(x) for x in RE_ARROW.split(signature))
    def multiplicity(code):
        m = RE_MULTIPLICITY.match(code)
        n, s = m.group(2), m.group(3)

        n = int(n) if n is not None else 1

        return (n, s)

    lhs, rhs = [[multiplicity(item) for item in x if item] for x in (lhs, rhs)]

    return lhs, rhs

