import os

from numpy import *
import scipy.constants as co
from numpy.polynomial.legendre import leggauss
from scipy.integrate import odeint, ode
from scipy.interpolate import interp1d

from matplotlib import pyplot as plt

from _glsausage import gl
from chemical import LeaderChemistry

rsmooth = frompyfunc(gl.rsmooth, 1, 1)

Rmax = 2 * co.milli
gl.rmax = Rmax

Td = 1e-17 * co.centi**2

MAXWELL_K = 1 / (4 * pi * co.epsilon_0)

nair = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')
n_N2 = 0.78 * nair
n_O2 = 0.22 * nair
EN_THRESHOLD = 120.0

mun_e = 1e24
mun_ominus = 1.2e22
mun_o2minus =  7.1e21 # pure o2: 5e21;  air: 7.1e21
mun_o3minus = 7.6e21
mun_no3minus = 6.6e21
MU = mun_e / nair

# Temporary ion mobiliy: use a better value
mun_o4plus = 5e21

def gl_init(ab_deg=24, r_deg=6, theta_deg=6):
    z, wz = leggauss(ab_deg)
    r, wr = leggauss(r_deg)
    theta, wtheta = leggauss(theta_deg)

    gl.set_gl_tables(z, wz, r, wr, theta, wtheta)


def fsigma(ne, no, no2, no3, o4plus):
    return (co.elementary_charge * mun_e * ne
            + co.elementary_charge * mun_ominus * no
            + co.elementary_charge * mun_o2minus * no2
            + co.elementary_charge * mun_o3minus * no3
            + co.elementary_charge * mun_o4plus * o4plus) / nair


class Sausage(object):
    def __init__(self):
        # Domain
        self.init_nodes = 10

        self.eb = 15 * co.kilo / co.centi
        self.ne0 = 1e20
        self.dt = 5e-12
        
        self.v = 4e6
        self.l = 0.5
        self.ztip0 = 1 * co.milli
        self.min_dz = 1e-4

        # This is an arbitrary value to obtain reasonable velocities.
        self.head_mobility = 1
        self.dip_z = 0.2
        self.dip_a = 0.15
        self.dip_width = 0.025
        
        self.diff = (self.v * self.dt)**2 / (co.nano)
        
        self.RS = LeaderChemistry()

        self.output_dt = 0.0

    
    def field_fluxes(self, zfluxes, q_s, ztip, reduced=True):
        phi = (MAXWELL_K * gl.fluxes(r_[zfluxes, ztip], q_s, ztip, zfluxes)
               + gl.bgfluxes(self.eb, ztip, zfluxes))

        R = rsmooth(ztip - zfluxes).astype('f')
        csections = pi * R**2
        E0 = phi / (csections + 1e-15)
        #E0 = where(E0 > self.eb, self.eb, E0)
        
        EN = abs(E0 / nair / Td)
        if reduced:
            return phi, EN
        else:
            return phi, E0
        
    def pack(self, ztip, n, lmbd_s):
        """ Takes the densities n and the lmbd_{s, c} and packs them into
        a 1d vector. """
        
        return r_[ztip, ravel(n), lmbd_s]
    
    def unpack(self, y):
        """ Inverse of 'pack'. m is the number of cells but since we do not
        integrate densities at ztip we have m densities and m charges.
        """
        m = len(self.zf)

        ztip, y = y[0], y[1:]
        n, q_s = (y[:m * self.RS.nspecies].reshape(self.RS.nspecies, m),
                  y[m * self.RS.nspecies:])
        
        return ztip, n, q_s

        
    def f(self, t, y, debug=False):
        """ Calculates derivatives with fixed nodes.  """
        ztip, n, q = self.unpack(y)

        
        ne, no, no2, no3, o4plus = [self.RS.get_species(n, s) for s in
                                    ('e', 'O-', 'O2-', 'O3-', 'O4+')]
        sigma = fsigma(ne, no, no2, no3, o4plus)
        
        phi, en = self.field_fluxes(self.zf, q, ztip)
        
        epeak = MAXWELL_K * gl.epeak(r_[self.zf, ztip], q, ztip)
        
        gamma = r_[sigma * phi, 0]
        dq = gamma[:-1] - gamma[1:]

        # This is an artificial diffusion to avoid problems presumably
        # related to ill-conditioning of the q evolution.
        dz = diff(r_[self.zf, ztip])        
        qdz = where(dz > 0, q / dz, 0)
        gamma_diff = r_[0.0,
                        -2 * self.diff * diff(qdz) / (dz[1:] + dz[:-1]),
                        0.0]
        
        dq += gamma_diff[:-1] - gamma_diff[1:]

        dn = self.RS.fderivs(n, en, 300)

        dztip = self.head_mobility * epeak

        dy = self.pack(dztip, dn, dq)
        if debug:
            embed()
            
        return dy

    def init(self):
        # Initial conditions
        self.zf = linspace(0, self.ztip0, self.init_nodes + 1)[:-1]
        self.ztip = self.ztip0
        
        self.q = zeros(self.init_nodes)
        self.n = zeros((self.RS.nspecies, self.init_nodes))
        
        self.RS.set_species(self.n, 'e', self.ne0)
        self.RS.set_species(self.n, 'N2+', self.ne0)

        self.t = 0.0

        
    def advance(self, dt):
        y0 = self.pack(self.ztip, self.n, self.q)

        r = ode(self.f)
        #r.set_integrator('vode', method='bdf', atol=1.0e15, rtol=1e-5)
        r.set_integrator('dopri5', atol=1.0e15, rtol=1e-5)
        r.set_initial_value(y0, self.t)

        endt = self.t + dt
        print("Integrating %g s -> %g s" % (self.t, endt))
        r.integrate(endt)
        assert(r.successful())
        self.ztip, self.n, self.q = self.unpack(r.y)
        self.t = endt
        
        del r

        
    def run(self, plot=False):
        n_boundary = zeros((self.RS.nspecies, 1))
        self.RS.set_species(n_boundary, 'e', self.ne0)
        self.RS.set_species(n_boundary, 'N2+', self.ne0)
        
        if plot:
            self.plot()

        out_nsteps = 40
        
        for i in range(5000):
            self.advance(self.dt)
            print("z: %g -> %g" % (self.zf[-1], self.ztip))
            
            if self.ztip - self.zf[-1] > self.min_dz:
                # We do not insert the new node exactly at ztip to avoid a
                # cell that momenterily is exaclty of length 0.  The epsilon
                # value should not be relevant as long as it is small since the
                # new cell contains 0 charge.
                epsilon_length = 1e-10
                self.zf = r_[self.zf, self.ztip - epsilon_length]
                self.q = r_[self.q, 0]
                self.n = concatenate((self.n, n_boundary), axis=1)
                
            if plot and i % out_nsteps == 0:
                self.save(i / out_nsteps)
                self.plot()

        
    def save(self, step):
        def fsave(var, x, y):
            path = os.path.expanduser("~/data/sausage/scratch")
            fname = os.path.join(path, '%s_%.5d.dat' % (var, step))
            savetxt(fname, c_[x, y])
        
        zp = r_[self.zf, self.ztip]
        zc = 0.5 * (zp[1:] + zp[:-1])
        dz = (zp[1:] - zp[:-1])

        fsave('charge', zc, self.q / dz)

        fsave('electrons', self.zf, self.RS.get_species(self.n, 'e'))

        ne, no, no2, no3, o4plus = [self.RS.get_species(self.n, s) for s in
                                    ('e', 'O-', 'O2-', 'O3-', 'O4+')]
        sigma = fsigma(ne, no, no2, no3, o4plus)
        fsave('sigma', self.zf, sigma)

        phi, E0 = self.field_fluxes(self.zf, self.q, self.ztip, reduced=True)
        fsave('en', self.zf, E0)

        
    def plot(self):
        zp = r_[self.zf, self.ztip]
        zc = 0.5 * (zp[1:] + zp[:-1])
        dz = (zp[1:] - zp[:-1])

        plt.figure('Charge density')
        plt.plot(zc, self.q / dz, c='k', alpha=0.7)

        plt.figure('Electron density')
        
        plt.plot(self.zf, self.RS.get_species(self.n, 'e'), c='k', alpha=0.7)

        plt.figure('Conductivity')
        ne, no, no2, no3, o4plus = [self.RS.get_species(self.n, s) for s in
                                    ('e', 'O-', 'O2-', 'O3-', 'O4+')]
        sigma = fsigma(ne, no, no2, no3, o4plus)

        plt.plot(self.zf, sigma, c='b', alpha=0.7)

        plt.figure('Reduced field')
        phi, E0 = self.field_fluxes(self.zf, self.q, self.ztip, reduced=True)
        plt.plot(self.zf, E0, c="#ff8800")

        
def main():
    gl_init()

    sausage = Sausage()
    sausage.init()

    sausage.run(plot=True)
    #sausage.plot()

    plt.show()
    
    
    
    
if __name__ == '__main__':
    main()
