#! /bin/bash
# Script to start a simulation in the qsub queue
# Alejandro Luque - 2012

PYTHON_EXEC=python
UNAME_FULL=`uname -a`
DATE=`date`

echo "# [${DATE}]"
echo "# ${UNAME_FULL}"

# export PYTHONPATH="${HOME}/lib/python:${HOME}/lib/python3.3/site-packages"

echo "Executing" ${PYTHON_EXEC} ${MAIN} ${INPUT_FILE}

#. ~/gcc5/use_gcc5.sh
export OMP_NUM_THREADS=${NTHREADS}
${PYTHON_EXEC} ${MAIN} ${INPUT_FILE}

DATE=`date`
echo "# [${DATE}]"
