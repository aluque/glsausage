from numpy import *
import scipy.constants as co

import chemise as ch

MUN_E = 1e24
MUN_OMINUS = 1.2e22
MUN_O2MINUS =  7.1e21 # pure o2: 5e21;  air: 7.1e21
MUN_O3MINUS = 7.6e21
MUN_NO3MINUS = 6.6e21

# Temporary ion mobiliy: use a better value
MUN_O4PLUS = 5e21
MUN_DEFAULT = 5.3e21

NAIR = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')
N_N2 = 0.78 * NAIR
N_O2 = 0.22 * NAIR

class HumidAir(ch.ReactionSet):
    
    DEFAULT_REDUCED_MOBILITIES = {
        'e'   : MUN_E,
        'O-'  : MUN_OMINUS,
        'O2-' : MUN_O2MINUS,
        'O3-' : MUN_O3MINUS,
        'O4+' : MUN_DEFAULT,
        'N4+' : MUN_DEFAULT,
        'N2+' : MUN_DEFAULT,
        'O2+' : MUN_DEFAULT,
        'O2+' : MUN_DEFAULT,
        'O2^-.(H2O)': MUN_DEFAULT,
        'O2^-.(H2O)2': MUN_DEFAULT,
        'O2^-.(H2O)3': MUN_DEFAULT,
        'O2+.(H2O)': MUN_DEFAULT,
        'H3O+': MUN_DEFAULT,
        'H3O+.(H2O)': MUN_DEFAULT,
        'H3O+.(H2O)2': MUN_DEFAULT,
        'H3O+.(H2O)3': MUN_DEFAULT,
    }
    
    def __init__(self, pw=0.015, extend=False):
        super(HumidAir, self).__init__()
        
        self.pw = pw
        
        # These are ignored products
        self.fix({
            'O': 0.0,
            'OH' : 0.0,
            'H': 0.0,
            'N2O': 0.0})

        self.compose({'M': {'N2': 1.0, 'O2': 1.0}})
                     
        self.add("e + N2 -> 2 * e + N2+",
                 ch.LogLogInterpolate0("swarm/k025.dat", extend=extend))

        self.add("e + O2 -> 2 * e + O2+", 
                 ch.LogLogInterpolate0("swarm/k042.dat", extend=extend))

        self.add("e + O2 + O2 -> O2- + O2",
                 ch.LogLogInterpolate0("swarm/k026.dat",
                                       prefactor=(1 / co.centi**-3),
                                       extend=extend))

        self.add("e + O2 -> O + O-",
                 ch.LogLogInterpolate0("swarm/k027.dat"), extend=extend)

        self.add("M + O2- -> e + O2 + M",
                 PancheshnyiFitEN(1.24e-11 * co.centi**3, 179, 8.8),
                 ref="Pancheshnyi2013/JPhD")
               
        self.add("O2 + O- -> O2- + O",
                 PancheshnyiFitEN(6.96e-11 * co.centi**3, 198, 5.6),
                 ref="Pancheshnyi2013/JPhD")

        self.add("N2 + O- -> e + N2O",
                 PancheshnyiFitEN(1.16e-12 * co.centi**3, 48.9, 11),
                 ref="Pancheshnyi2013/JPhD")

        self.add("O2 + O- + M -> O3- + M",
                 PancheshnyiFitEN2(1.1e-30 * co.centi**6, 65),
                 ref="Pancheshnyi2013/JPhD")

        # This is the g in Gallimberti 1979 fitted to experimental data
        # and transformed into Td / K.  See scratch/gallimberti/gallimberti.py
        g = 0.18
        
        self.add2("O2- + H2O + M -> O2^-.(H2O) + M",
                  ch.Constant(2.2e-28 * co.centi**6),
                  Gallimberti(2.2e-28 * co.centi**6, NAIR,
                              -18.44 * co.kilo * co.calorie / co.N_A, g),
                  #ch.Constant(2e-22 * co.centi**3),
                  ref="Gallimberti1979/JPhys")

        self.add2("O2^-.(H2O) + H2O + M -> O2^-.(H2O)2 + M",
                  ch.Constant(5e-28 * co.centi**6),
                  Gallimberti(5e-28 * co.centi**6, NAIR,
                              -8.35 * co.kilo * co.calorie / co.N_A, g),
                  #ch.Constant(1.1e-14 * co.centi**3),
                  ref="Gallimberti1979/JPhys")

        self.add2("O2^-.(H2O)2 + H2O + M -> O2^-.(H2O)3 + M",
                  ch.Constant(5e-29 * co.centi**6),
                  Gallimberti(5e-29 * co.centi**6, NAIR,
                              -6.46 * co.kilo * co.calorie / co.N_A, g),
                  #ch.Constant(9e-15 * co.centi**3),
                  ref="Gallimberti1979/JPhys")
        
        self.add("N2+ + N2 + M -> N4+ + M",
                 TemperaturePower(5e-29 * co.centi**6, 3),
                 ref="Aleksandrov1999/PSST")
        
        self.add("N4+ + O2 -> 2 * N2 + O2+",
                 TemperaturePower(2.5e-10 * co.centi**3, 3),
                 ref="Aleksandrov1999/PSST")


        self.add("O2+ + O2 + M -> O4+ + M",
                 TemperaturePower(2.4e-30 * co.centi**6, 3),
                 ref="Aleksandrov1999/PSST")


        self.add("O2+ + H2O + M -> O2+.(H2O) + M",
                 ch.Constant(2.6e-28 * co.centi**6),
                 ref="Aleksandrov1999/PSST")

        self.add("O2+.(H2O) + H2O -> H3O+ + OH + O2",
                 ch.Constant(3e-10 * co.centi**3),
                 ref="Aleksandrov1999/PSST")

        def cluster(n):
            if n == 0:
                return "H3O+"
            if n == 1:
                return "H3O+.(H2O)"
            else:
                return "H3O+.(H2O)%d" % n

            
        for n in range(3):
            self.add("%s + H2O + M -> %s + M" % (cluster(n), cluster(n + 1)),
                     ch.Constant(3e-27 * co.centi**6),
                     ref="Aleksandrov1999/PSST")
        
        self.add("e + H3O+.(H2O)3 -> H + 4 * H2O",
                 ETemperaturePower(6.5e-6 * co.centi**3, 0.5,
                                   "swarm/meanenergy.dat"),
                 ref="Aleksandrov1999/PSST")
        
        self.add("e + O4+ -> ",
                 ch.Interpolate0("swarm/rec_electron.dat", zero_value=0.0,
                                 extend=extend))
        
        # Add bulk recombination reactions
        pos = [s for s in self.species if '+' in s] 
        neg = [s for s in self.species if '-' in s] 
        self.add_pattern("{pos} + {neg} -> ",
                         {'pos': pos, 'neg': neg},
                         ch.Constant(1e-7 * co.centi**3),
                         generic="A+ + B- -> ", 
                         ref="Kossyi1992/PSST")
        
                 
        self.initialize()


    def init_neutrals(self, n):
        self.set_species(n, 'N2', N_N2)
        self.set_species(n, 'O2', N_O2)
        self.set_species(n, 'H2O', self.pw * NAIR)


class DryAir(ch.ReactionSet):
    DEFAULT_REDUCED_MOBILITIES = {
        'e'   : MUN_E,
        'O-'  : MUN_OMINUS,
        'O2-' : MUN_O2MINUS,
        'O3-' : MUN_O3MINUS,
        'O4+' : MUN_O4PLUS}

    def __init__(self, extend=False):
        super(DryAir, self).__init__()
        
        self.compose({'M': {'N2': 1.0, 'O2': 1.0}})

        self.fix({
            'O': 0.0,
            'N2O': 0.0})

        self.add("e + N2 -> 2 * e + N2+",
                 ch.LogLogInterpolate0("swarm/k025.dat", extend=extend))

        self.add("e + O2 -> 2 * e + O2+", 
                 ch.LogLogInterpolate0("swarm/k042.dat", extend=extend))

        self.add("e + O2 + O2 -> O2- + O2",
                 ch.LogLogInterpolate0("swarm/k026.dat",
                                       prefactor=(1 / co.centi**-3),
                                       extend=extend))

        self.add("e + O2 -> O + O-",
               ch.LogLogInterpolate0("swarm/k027.dat", extend=extend))

        self.add("M + O2- -> e + O2 + M",
               PancheshnyiFitEN(1.24e-11 * co.centi**3, 179, 8.8))
               
        self.add("O2 + O- -> O2- + O",
               PancheshnyiFitEN(6.96e-11 * co.centi**3, 198, 5.6))

        self.add("N2 + O- -> e + N2O",
               PancheshnyiFitEN(1.16e-12 * co.centi**3, 48.9, 11))

        self.add("O2 + O- + M -> O3- + M",
               PancheshnyiFitEN2(1.1e-30 * co.centi**6, 65))

        # Let us simplify and assume that all + ions are converted to O4+
        # within 1 us

        self.add("N2+ -> O4+", ch.Constant(1e9))
        self.add("O2+ -> O4+", ch.Constant(1e9))
        
        self.add("e + O4+ -> ",
               ch.Interpolate0("swarm/rec_electron.dat", zero_value=0.0,
                               extend=extend))
        
        self.add("O- + O4+ + M -> ",
               ch.Interpolate0("swarm/rec_ion.dat",
                               zero_value=0.0, extend=extend))
        
        self.add("O2- + O4+ + M -> ",
               ch.Interpolate0("swarm/rec_ion.dat",
                               zero_value=0.0, extend=extend))
        
        self.add("O3- + O4+ + M -> ",
               ch.Interpolate0("swarm/rec_ion.dat",
                               zero_value=0.0, extend=extend))
        
        self.initialize()


    def init_neutrals(self, n):
        self.set_species(n, 'N2', N_N2)
        self.set_species(n, 'O2', N_O2)


        
class IonizationAttachment(ch.ReactionSet):
    DEFAULT_REDUCED_MOBILITIES = {
        'e'   : MUN_E,
    }

    def __init__(self):
        super(IonizationAttachment, self).__init__()
        
        self.fix({
            'N2': N_N2,
            'O2': N_O2,
            'M': NAIR,
            'O': 0.0,
            'N2O': 0.0})

        self.add("e + N2 -> 2 * e + N2+",
                 ch.LogLogInterpolate0("swarm/k025.dat"))

        self.add("e + O2 -> 2 * e + O2+", 
                 ch.LogLogInterpolate0("swarm/k042.dat"))

        self.add("e + O2 + O2 -> O2- + O2",
                 ch.LogLogInterpolate0("swarm/k026.dat",
                                       prefactor=(1 / co.centi**-3)))

        self.add("e + O2 -> O + O-",
               ch.LogLogInterpolate0("swarm/k027.dat"))
        
        self.initialize()

        
    def init_neutrals(self, n):
        self.set_species(n, 'N2', N_N2)
        self.set_species(n, 'O2', N_O2)

        
        
class TemperaturePower(ch.Rate):
    def __init__(self, k0, power, T0=300):
        self.k0 = k0
        self.power = power
        self.T0 = T0

    def __call__(self, EN, T):
        return full_like(EN, self.k0 * (self.T0 / T)** self.power)

    def latex(self):
        return (r"$\num{%g} \times (\num{%g} / T)^{\num{%g}}$"
                % (self.k0, self.T0, self.power))


class ETemperaturePower(ch.LogLogInterpolate0):
    def __init__(self, k0, power, *args, T0=300, **kwargs):
        self.k0 = k0
        self.power = power
        self.T0 = T0

        super(ETemperaturePower, self).__init__(*args, **kwargs)
        

    def __call__(self, EN, T):
        Te = 2 * exp(self.s(log(EN))) / (3 * co.k)
        return full_like(EN, self.k0 * (self.T0 / Te)** self.power)

    def latex(self):
        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"
                % (self.k0, self.T0, self.power))


class PancheshnyiFitEN(ch.Rate):
    def __init__(self, k0, a, b):
        self.k0 = k0
        self.a = a
        self.b = b

    def __call__(self, EN, T):
        return self.k0 * exp(-(self.a / (self.b + EN))**2)

    def latex(self):
        self.k_0 = self.k0
        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))
                  for s in ('k_0', 'a', 'b')]

        return ("$k_0e^{-\\left(\\frac{a}{b + E/n}\\right)^2}$ [%s]"
                % ", ".join(params))


class PancheshnyiFitEN2(ch.Rate):
    def __init__(self, k0, a):
        self.k0 = k0
        self.a = a

    def latex(self):
        self.k_0 = self.k0
        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))
                  for s in ('k_0', 'a')]

        return ("$k_0e^{-\\left(\\frac{E/n}{a}\\right)^2}$ [%s]"
                % ", ".join(params))

    def __call__(self, EN, T):
        return self.k0 * exp(-(EN / self.a)**2)


class Gallimberti(ch.Rate):
    def __init__(self, kforward, n0, deltag, g):
        self.k0 = kforward * n0
        self.deltag = deltag
        self.g = g

    def latex(self):
        params = ["$%s = \\num{%.3g}$" % (s, v)
                  for s, v in (('k_0', self.k0),
                               ('\Delta G', self.deltag))]

        return ("$k_0 e^{\\left(\\frac{\Delta G}{kT_i}\\right)}$ [%s]"
                % ", ".join(params))
        # return ("$k_0 e^{\\left(\\frac{\Delta G}{kT_i}\\right)}; T_i=T + \\frac{1}{g}\\frac{E}{n}$ [%s]"
        #         % ", ".join(params))
        

    def __call__(self, EN, T):
        Ti = T + EN / self.g
        
        return self.k0 * exp(self.deltag / (co.k * Ti))
