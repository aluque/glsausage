module gl
  implicit none
  public
  
  double precision, parameter :: pi = 3.141592653589793D0

  double precision :: rmax
  double precision :: electrode_r
  double precision :: radius_growth_rate
  double precision :: charge_layer_width
  
  double precision, parameter :: epsilon = 1.0e-10
  double precision, parameter :: high_order_threshold = 5e-1
  double precision, parameter :: epsilon_r = 1.0e-5
  logical, parameter :: ignore_slopes = .true.

  ! The Gauss-Legendre Integration points and weights for the integrations
  ! in z, r, and theta
  double precision, allocatable, target :: gl_z(:), gl_wz(:)
  double precision, allocatable, target :: gl_r(:), gl_wr(:)
  double precision, allocatable, target :: gl_theta(:), gl_wtheta(:)
  double precision, allocatable, target :: gl_zn(:), gl_wzn(:)
  double precision, allocatable, target :: gl_zx(:), gl_wzx(:)
  double precision, allocatable, target :: gl_thetan(:), gl_wthetan(:)
  integer :: gl_nz, gl_nr, gl_ntheta, gl_nzn, gl_nthetan, gl_nzx
  logical :: gl_allocated = .false.
contains


subroutine set_gl_tables(n, z, wz, m, r, wr, l, theta, wtheta,&
     & nn, zn, wzn, ln, thetan, wthetan, nx, zx, wzx)
  ! Sets the Gauss-Legendre tables for later use.  The *n parameters
  ! are for neighboring cells, which require better resolution due to
  ! the singularity at r=0, theta=0.
  integer :: n, m, l, nn, ln, nx
  double precision, intent(in) :: z(n), wz(n), r(m), wr(m), theta(l), wtheta(l)
  double precision, intent(in) :: zn(nn), wzn(nn), thetan(ln), wthetan(ln)
  double precision, intent(in) :: zx(nx), wzx(nx)

  !f2py integer intent(hide), depend(z) :: n=len(z)
  !f2py integer intent(hide), depend(r) :: m=len(r)
  !f2py integer intent(hide), depend(theta) :: l=len(theta)
  !f2py integer intent(hide), depend(zn) :: nn=len(zn)
  !f2py integer intent(hide), depend(thetan) :: ln=len(thetan)
  !f2py integer intent(hide), depend(zx) :: nx=len(zx)

  if (gl_allocated) then
     deallocate(gl_z)
     deallocate(gl_wz)

     deallocate(gl_zn)
     deallocate(gl_wzn)

     deallocate(gl_zx)
     deallocate(gl_wzx)
     
     deallocate(gl_r)
     deallocate(gl_wr)

     deallocate(gl_theta)
     deallocate(gl_wtheta)

     deallocate(gl_thetan)
     deallocate(gl_wthetan)

  end if
  
  allocate(gl_z(n))
  allocate(gl_wz(n))
  
  allocate(gl_zn(nn))
  allocate(gl_wzn(nn))

  allocate(gl_zx(nx))
  allocate(gl_wzx(nx))

  allocate(gl_r(m))
  allocate(gl_wr(m))
  
  allocate(gl_theta(l))
  allocate(gl_wtheta(l))
  
  allocate(gl_thetan(ln))
  allocate(gl_wthetan(ln))
  
  gl_allocated = .true.

  gl_z = z
  gl_wz = wz
  gl_nz = n

  gl_r = r
  gl_wr = wr
  gl_nr = m

  gl_theta = theta
  gl_wtheta = wtheta
  gl_ntheta = l

  gl_zn = zn
  gl_wzn = wzn
  gl_nzn = nn

  gl_thetan = thetan
  gl_wthetan = wthetan
  gl_nthetan = ln

  gl_zx = zx
  gl_wzx = wzx
  gl_nzx = nx
  
end subroutine set_gl_tables


double precision function green(R, R0, theta, deltaz)
  ! Green's function for the effect of a charge element.
  double precision, intent(in) :: deltaz, R0, theta, R
  double precision :: rabs

  rabs = sqrt((R0 - R * cos(theta))**2 + R**2 * sin(theta)**2 + deltaz**2)

  green = deltaz / rabs**3
  return
end function green


double precision function green_axis(R, deltaz)
  ! Green's function for the effect of a charge element on the axis
  ! (i.e. x=0 in 'green' above).  This factors out the theta integral
  ! and introduces a factor 2 * pi that we include here.
  
  double precision, intent(in) :: deltaz, R
  double precision :: rabs

  rabs = sqrt(R**2 + deltaz**2)

  green_axis = 2 * pi * deltaz / rabs**3
  return
end function green_axis


double precision function greenr(R, R0, theta, deltaz)
  ! The previous function after integrating over 2 pi x dx
  double precision, intent(in) :: deltaz, theta, R, R0
  double precision :: z, z2, a, costheta, cos2theta, v, k, l, m

  if (R .eq. 0) then
     greenr = 0.0d0
     return
  end if
  
  z = deltaz / R
  z2 = z*z
  a = R0 / R
  costheta = cos(theta)
  cos2theta = cos(2 * theta)
  
  v = sqrt(1 + a**2 + z2 - 2 * a * costheta)
  k = a * costheta - z2 - 1
  l = sqrt(1 + z2)
  m = cos2theta - 2 * z2 - 1
  

  greenr = -4 * pi * z * (k + l * v) / (v * m)
  return
end function greenr


double precision function green_layer(R, R0, R1, theta, deltaz, rstar, rstar_tip)
  double precision, intent(in) :: deltaz, theta, R, R0, R1, rstar, rstar_tip
  double precision :: eplus, emin

  if (R .eq. 0 .or. R0 .eq. 0) then
     ! Note that the limit of this function as R0->0 is not neccesarily 0
     ! but that is the case corresponding to ztip and we do not allow
     ! any charge to exit the streamer.
     green_layer = 0.0d0
     return
  end if

  eplus = green(R, R0 * (1 + epsilon_r), theta, deltaz)
  emin  = green(R, R0 * (1 - epsilon_r), theta, deltaz)
  
  green_layer = pi * (R1 - R0) * (eplus * (R0 + R1) + emin * (3 * R0 + R1))/6.
  
end function green_layer


double precision function rsmooth(xi, rstar_tip)
  double precision, intent(in) :: xi, rstar_tip

  rsmooth = sqrt(1 - exp(-xi / rstar_tip))
end function rsmooth


double precision function flux(a, b, rho, alpha, z0, ztip, &
     rstar_a, rstar_b, rstar_0, rstar_tip, delta)
  ! Calculates the flux at a given point z0 created by a tube section between
  ! a and b.  rho is the average charge per meter and radian.
  ! alpha is the slope of the first-order approximation to the actual rho
  ! i.e. the rho(z) assumed is rho(z) = (rho + alpha * (z - zmid))
  ! rstar_* is the "radius correction" at points (a, b, z0). This is, the actual
  ! radius is rsmooth(z, rstar_tip) * rstar.  rstar is linearly interpolated
  ! between a and b.
  
  double precision, intent(in) :: a, b, rho, alpha, z0, ztip
  double precision, intent(in) :: rstar_a, rstar_b, rstar_0, rstar_tip, delta
  double precision w, rr, theta, z, zmid, rstar
  double precision R, R0, R1, dl, g, g1, lf, qf
  
  integer :: i, j, gl_nz0, gl_ntheta0
  double precision, pointer ::  gl_z0(:), gl_wz0(:), gl_theta0(:), gl_wtheta0(:)
    
  gl_nz0 = gl_nz
  gl_z0 => gl_z
  gl_wz0 => gl_wz
     
  gl_ntheta0 = gl_ntheta
  gl_theta0 => gl_theta
  gl_wtheta0 => gl_wtheta
  
  if (abs(a - z0) < rmax * high_order_threshold &
       & .or. abs(b - z0) < rmax * high_order_threshold) then
     ! We have to use higher resolution bc we are dealing with neighbouring
     ! cells.
     gl_nz0 = gl_nzn
     gl_z0 => gl_zn
     gl_wz0 => gl_wzn
     
     gl_ntheta0 = gl_nthetan
     gl_theta0 => gl_thetan
     gl_wtheta0 => gl_wthetan
  end if

  R0 = rsmooth(ztip - z0, rstar_tip) * rstar_0
  R1 = rsmooth(ztip - z0 + delta, rstar_tip) * rstar_0
  zmid = 0.5 * (a + b)
  
  flux = 0.0
  
  do i=1, gl_nz0
     z = a + (b - a) * (1 + gl_z0(i)) / 2.0
     rstar = rstar_b * (z - a) / (b - a) + rstar_a * (b - z) / (b - a)
     R  = rsmooth(ztip - z, rstar_tip) * rstar
     do j=1, gl_ntheta0
        theta = pi * (1 + gl_theta0(j))
        w = gl_wz0(i) * (b - a) * gl_wtheta0(j) * pi / 2.0

        ! Optimization chance: we do not need to use higher orders for
        ! the image charges.
        g = greenr(R, R0, theta, z0 - z) &
             + green_layer(R, R0, R1, theta, z0 - z, rstar, rstar_tip)

        ! if ((b .eq. ztip) .and. (abs(z0 - ztip) .lt. 0.0001) &
        !      .and. (green_layer(R, R0, R1, theta, z0 - z, rstar, rstar_tip) .ne. 0)) then
        !    write (*,*) greenr(R, R0, theta, z0 - z), green_layer(R, R0, R1, theta, z0 - z, rstar, rstar_tip)
        ! end if
        ! Distances in the image are squeezed by this factor
        lf = electrode_r**2 / (R**2 + z**2)

        ! We have to multiply linear charge densities by this factor
        ! when we calculate the effect of mirror charges.
        qf = -sqrt(lf)

        g1 = qf * greenr(lf * R, R0, theta, z0 - lf * z) &
             + qf * green_layer(lf * R, R0, R1, theta, z0 - lf * z, &
             rstar, rstar_tip)

        flux = flux + w * (g + g1) * (rho + alpha * (z - zmid))
     end do
  end do
end function flux


double precision function eaxis1(a, b, rho, alpha, z0, ztip, &
     & rstar_a, rstar_b, rstar_tip)
  ! Calculates the e-field at the axis at a single point.

  double precision, intent(in) :: a, b, rho, alpha, z0, ztip
  double precision, intent(in) :: rstar_a, rstar_b, rstar_tip

  double precision w, theta, z, zmid, rstar
  double precision R, dl, g, g1, lf, qf
  double precision, pointer ::  gl_z0(:), gl_wz0(:)

  integer :: i, j, k, gl_nz0

  gl_nz0 = gl_nz
  gl_z0 => gl_z
  gl_wz0 => gl_wz

  if (abs(a - z0) < rmax * high_order_threshold &
       & .or. abs(b - z0) < rmax * high_order_threshold) then
     ! We have to use higher resolution bc we are dealing with neighbouring
     ! cells.
     gl_nz0 = gl_nzx
     gl_z0 => gl_zx
     gl_wz0 => gl_wzx
  end if
  
  zmid = 0.5 * (a + b)
  eaxis1 = 0.0
  do i=1, gl_nz0
     z = a + (b - a) * (1 + gl_z0(i)) / 2.0
     if (z .lt. ztip) then
        rstar = rstar_b * (z - a) / (b - a) + rstar_a * (b - z) / (b - a)
        R = rsmooth(ztip - z, rstar_tip) * rstar

        w = gl_wz0(i) * (b - a) / 2.0
           
        ! The Green's function takes into account the mirror charges
        g = green_axis(R, z0 - z)
        
        lf = electrode_r**2 / (R**2 + z**2)
        qf = -sqrt(lf)

        g1 = qf * green_axis(lf * R, z0 - lf * z)
        if (.not. ignore_slopes) then
           eaxis1 = eaxis1 + w * (g + g1) * (rho + alpha * (z - zmid))
        else
           eaxis1 = eaxis1 + w * (g + g1) * rho
        end if
     end if
  end do
end function eaxis1


subroutine eaxis(n, zf, q, ztip, m, zfluxes, ez, rstar, rstar_tip)
  integer, intent(in) :: n, m
  double precision, intent(in) :: ztip, zf(n + 1), q(n), zfluxes(m)
  double precision, intent(in) :: rstar(n + 1), rstar_tip
  double precision, intent(out) :: ez(m)

  !f2py integer intent(hide), depend(q) :: n=len(q)
  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i, j
  double precision :: z0, dz(n), rho(n), alpha(n), zmid(n)

  dz = zf(2: n + 1) - zf(1: n)
  
  rho = q / dz / (2 * pi)
  if (.not. ignore_slopes) then
     alpha = slopes(n, zf, rho)
  end if
  
  !$OMP PARALLEL DO PRIVATE(j, z0)
  do i=1, m
     z0 = zfluxes(i)
     do j=1, n
        ez(i) = ez(i) + eaxis1(zf(j), zf(j + 1), rho(j), alpha(j), z0, ztip, &
             rstar(j), rstar(j + 1), rstar_tip)
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine eaxis


double precision function eoff1(a, b, rho, alpha, z0, R0, ztip, &
     & rstar_a, rstar_b, rstar_tip)
  ! Calculates the e-field at the axis at a single point.

  double precision, intent(in) :: a, b, rho, alpha, z0, R0, ztip
  double precision, intent(in) :: rstar_a, rstar_b, rstar_tip

  double precision w, theta, z, zmid, rstar
  double precision R, dl, g, g1, lf, qf

  integer :: i, j, k
  integer :: gl_nz0, gl_ntheta0
  double precision, pointer ::  gl_z0(:), gl_wz0(:), gl_theta0(:), gl_wtheta0(:)
    
  gl_nz0 = gl_nz
  gl_z0 => gl_z
  gl_wz0 => gl_wz
     
  gl_ntheta0 = gl_ntheta
  gl_theta0 => gl_theta
  gl_wtheta0 => gl_wtheta
  
  if (abs(a - z0) < rmax * high_order_threshold &
       & .or. abs(b - z0) < rmax * high_order_threshold) then
     ! We have to use higher resolution bc we are dealing with neighbouring
     ! cells.
     gl_nz0 = gl_nzn
     gl_z0 => gl_zn
     gl_wz0 => gl_wzn
     
     gl_ntheta0 = gl_nthetan
     gl_theta0 => gl_thetan
     gl_wtheta0 => gl_wthetan
  end if

  
  zmid = 0.5 * (a + b)
  eoff1 = 0.0
  do i=1, gl_nz0
     z = a + (b - a) * (1 + gl_zn(i)) / 2.0
     rstar = rstar_b * (z - a) / (b - a) + rstar_a * (b - z) / (b - a)
     R = rsmooth(ztip - z, rstar_tip) * rstar
     do j=1, gl_ntheta0
        theta = pi * (1 + gl_theta0(j))
        w = gl_wz0(i) * (b - a) * gl_wtheta0(j) * pi / 2.0

        ! The Green's function takes into account the mirror charges
        g = green(R, R0, theta, z0 - z)
        
        lf = electrode_r**2 / (R**2 + z**2)
        qf = -sqrt(lf)

        g1 = qf * green(lf * R, R0, theta, z0 - lf * z)

        eoff1 = eoff1 + w * (g + g1) * (rho + alpha * (z - zmid))
     end do
  end do
end function eoff1


subroutine eoff(n, zf, q, R0, ztip, m, zfluxes, ez, rstar, rstar_tip)
  integer, intent(in) :: n, m
  double precision, intent(in) :: ztip, R0, zf(n + 1), q(n), zfluxes(m)
  double precision, intent(in) :: rstar(n + 1), rstar_tip
  double precision, intent(out) :: ez(m)

  !f2py integer intent(hide), depend(q) :: n=len(q)
  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i, j
  double precision :: z0, dz(n), rho(n), alpha(n), zmid(n)

  dz = zf(2: n + 1) - zf(1: n)
  
  rho = q / dz / (2 * pi)
  alpha = slopes(n, zf, rho)

  !$OMP PARALLEL DO PRIVATE(j, z0)
  do i=1, m
     z0 = zfluxes(i)
     do j=1, n
        ez(i) = ez(i) + eoff1(zf(j), zf(j + 1), rho(j), alpha(j), z0, R0, ztip, &
             rstar(j), rstar(j + 1), rstar_tip)
     end do
  end do
  !$OMP END PARALLEL DO

end subroutine eoff


subroutine fluxes(n, zf, q, ztip, m, zfluxes, sigma, phi, &
     rstar, rstarfluxes, rstar_tip, delta)
  integer, intent(in) :: n, m
  double precision, intent(in) :: ztip, zf(n + 1), q(n), zfluxes(m), sigma(m)
  double precision, intent(in) :: rstar(n + 1), rstarfluxes(m), rstar_tip, delta
  double precision, intent(out) :: phi(m)

  !f2py integer intent(hide), depend(q) :: n=len(q)
  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i, j
  double precision :: z0, dz(n), rho(n), alpha(n), rstar0

  dz = zf(2: n + 1) - zf(1: n)
  
  rho = q / dz / (2 * pi)
  alpha = slopes(n, zf, rho)

  phi(:) = 0.0
  !$OMP PARALLEL DO PRIVATE(j, z0, rstar0)
  do i=1, m
     z0 = zfluxes(i)
     rstar0 = rstarfluxes(i)
     do j=1, n
        phi(i) = phi(i) &
             + sigma(i) * flux(zf(j), zf(j + 1), rho(j), alpha(j), z0, ztip, &
             rstar(j), rstar(j + 1), rstar0, rstar_tip, delta)
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine fluxes


subroutine flux_matrix(n, zf, ztip, m, zfluxes, sigma, &
     rstar, rstarfluxes, a, rstar_tip, delta)
  integer, intent(in) :: n, m
  double precision, intent(in) :: ztip, zf(n + 1), zfluxes(m), sigma(m)
  double precision, intent(in) :: rstar(n + 1), rstarfluxes(m), rstar_tip, delta
  double precision, intent(out) :: a(n, m)

  !f2py integer intent(hide), depend(zf) :: n=len(zf) - 1
  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i, j
  double precision :: z0, rstar0, dz

  !$OMP PARALLEL DO PRIVATE(j, z0, dz, rstar0)
  do i=1, m
     z0 = zfluxes(i)
     rstar0 = rstarfluxes(i)
     do j=1, n
        dz = zf(j + 1) - zf(j)
        a(j, i) = sigma(i) * flux(zf(j), zf(j + 1), 1.0d0, 0.0d0, z0, ztip, &
             rstar(j), rstar(j + 1), rstar0, rstar_tip, delta) / (2 * pi * dz)
     end do
  end do
  !$OMP END PARALLEL DO
end subroutine flux_matrix


subroutine dq_matrix(n, zf, ztip, m, zfluxes, sigma, &
     rstar, rstarfluxes, w, rstar_tip, delta)
  integer, intent(in) :: n, m
  double precision, intent(in) :: ztip, zf(n + 1), zfluxes(m), sigma(m)
  double precision, intent(in) :: rstar(n + 1), rstarfluxes(m), rstar_tip, delta
  double precision, intent(out) :: w(n, m - 1)
  double precision :: a(n, m)
  !f2py integer intent(hide), depend(zf) :: n=len(zf) - 1
  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)
  
  integer :: i

  call flux_matrix(n, zf, ztip, m, zfluxes, sigma, rstar, &
       rstarfluxes, a, rstar_tip, delta)

  a(:, m) = 0
  
  !$OMP PARALLEL DO 
  do i=1, m - 1
     w(:, i) = a(:, i) - a(:, i + 1)
  end do
  !$OMP END PARALLEL DO
end subroutine dq_matrix


double precision function epeak(n, zf, q, ztip, rstar, rstar_tip, epsilon)
  ! Calculates the electric field at the streamer tip.
  integer, intent(in) :: n
  double precision, intent(in) :: ztip, zf(n + 1), q(n), rstar(n + 1), rstar_tip
  double precision, intent(in) ::  epsilon
  
  !f2py integer intent(hide), depend(q) :: n=len(q)

  integer :: i, j
  double precision :: z0, dz(n), rho(n), alpha(n), zmid(n)

  dz = zf(2: n + 1) - zf(1: n)
  
  rho = q / dz / (2 * pi)
  if (.not. ignore_slopes) then
     alpha = slopes(n, zf, rho)
  end if
  epeak = 0
  
  do j=1, n
     dz = zf(j + 1) - zf(j)
     epeak = epeak + eaxis1(zf(j), zf(j + 1), rho(j), alpha(j), &
          ztip + epsilon, ztip, rstar(j), rstar(j + 1), rstar_tip)
  end do
end function epeak


function slopes(n, zf, rho)
  ! Calculates slopes using a slope-limiter
  integer, intent(in) :: n
  double precision, intent(in) :: zf(n + 1), rho(n)

  !f2py integer intent(hide), depend(rho) :: n=len(rho)

  double precision :: dz(n), zmid(0: n + 1), rhox(0: n + 1)
  double precision :: alphaf(n + 1)
  double precision :: slopes(n)
  integer :: i
  
  if (n .eq. 1) then
     slopes(1) = 0.0d0
     return
  end if
  
  zmid(1: n) = 0.5 * (zf(2: n + 1) + zf(1: n))
  zmid(0) = 2 * zmid(1) - zmid(2)
  zmid(n + 1) = 2 * zmid(n) - zmid(n - 1)
  
  rhox(1: n) = rho
  rhox(0) = rho(1)
  rhox(n + 1) = -rho(n)
  
  ! Centered differences
  ! alphac = (rhox(2: n + 1) - rhox(0: n - 1)) &
  !      / (zmid(2: n + 1) - zmid(0: n - 1))

  ! Left and right differences
  alphaf = (rhox(1: n + 1) - rhox(0: n)) / (zmid(1: n + 1) - zmid(0: n)) 
  
  !$OMP PARALLEL DO
  do i=1, n
     if (alphaf(i) * alphaf(i + 1) < 0) then
        slopes(i) = 0.0d0
     else if (abs(alphaf(i)) < abs(alphaf(i + 1))) then
        slopes(i) = alphaf(i)
     else
        slopes(i) = alphaf(i + 1)
     end if
  end do
  !$OMP END PARALLEL DO
end function slopes

! This is a subroutine wrapper for f2py, which does not like array-valued
! functions
subroutine fslopes(n, zf, rho, s)
  integer, intent(in) :: n
  double precision, intent(in) :: zf(n + 1), rho(n)
  double precision, intent(out) :: s(n)

  !f2py integer intent(hide), depend(rho) :: n=len(rho)
  s(:) = slopes(n, zf, rho)
  
end subroutine fslopes

subroutine electrode_fluxes(electrode_v, ztip, m, zfluxes, phi, &
     rstar, rstar_tip, delta)
  integer, intent(in) :: m
  double precision, intent(in) :: electrode_v, ztip, zfluxes(m)
  double precision, intent(in) :: rstar(m), rstar_tip, delta
  double precision, intent(out) :: phi(m)

  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i
  double precision :: z0, R0, a, d

  phi(:) = 0

  a = 2 * pi * electrode_r * electrode_v
  
  !$OMP PARALLEL DO PRIVATE(z0, R0, d)
  do i=1, m
     z0 = zfluxes(i)
     ! We are re-calculating this: optimize this out!
     R0 = rsmooth(ztip - z0 + delta, rstar_tip) * rstar(i)
     d = sqrt(z0**2 + R0**2)

     phi(i) = a * (1.0 - z0 / d)

  end do
  !$OMP END PARALLEL DO
end subroutine electrode_fluxes


subroutine uniform_fluxes(e0, ztip, m, zfluxes, phi, rstar, rstar_tip, delta)
  integer, intent(in) :: m
  double precision, intent(in) :: e0, ztip, zfluxes(m), rstar(m), rstar_tip, &
       delta
  double precision, intent(out) :: phi(m)

  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i
  double precision :: z0, R0, R1

  phi(:) = 0

  
  !$OMP PARALLEL DO PRIVATE(z0, R0)
  do i=1, m
     z0 = zfluxes(i)
     ! We are re-calculating this: optimize this out!
     R0 = rsmooth(ztip - z0 + delta, rstar_tip) * rstar(i)
     
     phi(i) = R0**2
  end do
  !$OMP END PARALLEL DO
  phi(:) = pi * e0 * phi(:)
  
end subroutine uniform_fluxes


subroutine pulse_fluxes(qpulse, zpulse, apulse, ztip, m, zfluxes, phi, &
     rstar, rstar_tip)
  integer, intent(in) :: m
  double precision, intent(in) :: qpulse, zpulse, apulse, ztip, zfluxes(m)
  double precision, intent(in) :: rstar(m), rstar_tip
  double precision, intent(out) :: phi(m)

  !f2py integer intent(hide), depend(zfluxes) :: m=len(zfluxes)

  integer :: i
  double precision :: z0, R0

  phi(:) = 0

  
  !$OMP PARALLEL DO PRIVATE(z0, R0)
  do i=1, m
     z0 = zfluxes(i)
     ! We are re-calculating this: optimize this out!
     R0 = rsmooth(ztip - z0, rstar_tip) * rstar(i)
     
     phi(i) = R0**2 * (z0 - zpulse) / ((z0 - zpulse)**2 + apulse**2)**1.5
  end do
  !$OMP END PARALLEL DO
  phi(:) = pi * qpulse * phi(:)
  
end subroutine pulse_fluxes

end module gl
