from numpy import *
from scipy.interpolate import UnivariateSpline

class Rate(object):
    def __call__(self, EN, T):
        raise NotImplementedError("You must specify the reaction rate")

    def asfortran(self):
        return "0.0/0.0"


class Constant(Rate):
    def __init__(self, k):
        self.k = k

    def __call__(self, EN, T):
        return self.k

    def asfortran(self):
        return str(self.k)


class PancheshnyiFitEN(object):
    def __init__(self, k0, a, b):
        self.k0 = k0
        self.a = a
        self.b = b

    def __call__(self, EN, T):
        return self.k0 * exp(-(self.a / (self.b + EN))**2)


class PancheshnyiFitEN2(object):
    def __init__(self, k0, a):
        self.k0 = k0
        self.a = a

    def __call__(self, EN, T):
        return self.k0 * exp(-(EN / self.a)**2)


class SplineEN(object):
    def __init__(self, fname, prefactor=1.0):
        en, k = loadtxt(fname, unpack=True)
        flt = k > 0
        
        self.s = UnivariateSpline(en[flt], log(prefactor * k[flt]))
        
    def __call__(self, EN, T):
        return exp(self.s(EN))
