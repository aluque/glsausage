#! /usr/bin/env python3.4
from numpy import *
import scipy.constants as co
import time

import h5py
from termcolor import colored, cprint

from main import Parameters

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", help="Input file")
    parser.add_argument("steps", nargs='?',
                        help="Step/steps to plot (empty for all)",
                        default='smart')

    args = parser.parse_args()

    ifile = h5py.File(args.ifile, "r")

    cprint("#\n# INPUT PARAMETERS: \n#", 'blue')
    gl = ifile["global"]
    params = Parameters()
    params.h5_load(gl)
    params.report()

    cprint("#\n# METADATA: \n#", 'blue')
    for key, item in gl.attrs.items():
        if key[0] == '_':
            pname = colored("%-20s" % key, 'red', attrs=['bold'])
            pvalue = "%-25s" % repr(item)
            cprint("%s = %s" % (pname, pvalue))


    steps_grp = ifile["steps"] 
    allsteps = list(steps_grp.keys())
    
    steps = select_steps(args.steps, allsteps)

    cprint("#\n# SAVED STEPS: \n#", 'blue')
    for i, step in enumerate(steps):
        g = steps_grp[step]
        t = array(g['t'])
        timestamp = g.attrs['_timestamp_']
        elapsed   = timestamp - gl.attrs['_timestamp_']
        ct = time.ctime(timestamp)
        cstep = colored(step, 'cyan', attrs=['bold'])
        celapsed = colored("[+%8.2f s]" % elapsed, 'green')
        print("%s \t%s %s \t%.4e s" % (cstep, ct, celapsed, t))
        

def select_steps(s, allsteps):
    if s == 'all':
        return allsteps

    if s == 'smart':
        steps = allsteps
        while len(steps) > 10:
            steps = steps[::10]

        return steps
    
    if s.startswith('[') and s.endswith(']'):
        s = s[1:-1]
    
    l = []
    for i, v in enumerate(s.split(':')):
        if v:
            l.append(int(v))
        else:
            l.append(None)
    if len(l) == 1:
        return [allsteps[l[0]]]
    else:
        _slice = slice(*l)
        return allsteps[_slice]

    
if __name__ == '__main__':
    main()
