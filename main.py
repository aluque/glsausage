#! /usr/bin/env python3.4

import os
import sys
import time
import importlib
import subprocess
import string
import itertools

from numpy import *
import scipy.constants as co
from scipy.interpolate import InterpolatedUnivariateSpline, interp1d, interp2d
from scipy.optimize import bisect, newton
from numpy.polynomial.legendre import leggauss
from scipy.linalg import solve, norm
import h5py

try:
    from IPython.core import ultratb
    sys.excepthook = ultratb.FormattedTB(mode='Verbose',
                                         color_scheme='Linux', call_pdb=1)
except ImportError:
    # No big deal if IPython is not installed
    pass

from _glsausage import gl
from param import ParamContainer, param, positive, contained_in

from termcolor import colored, cprint

Td = 1e-17 * co.centi**2

MAXWELL_K = 1 / (4 * pi * co.epsilon_0)
HEAD_MOBILITY = 1
Z_EPSILON = 1e-7

NAIR = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')
N_N2 = 0.78 * NAIR
N_O2 = 0.22 * NAIR


class Sausage(object):
    def __init__(self, name, parameters):
        self.parameters = parameters
        parameters.export(self)
        
        self.name = name
        self.fname = name + '.h5'        

        self.gl_init()

        # Import the chemistry module
        splitted = self.chemistry.split('.')
        mod, cls = '.'.join(splitted[:-1]), splitted[-1]
        chem_class = getattr(importlib.import_module(mod), cls)

        self.chem = chem_class(**self.chemistry_args)

        # The densities in the advancing boundary
        self.n0 = self.chem.zero_densities(1)

        self.chem.init_neutrals(self.n0)
        self.chem.set_species(self.n0, 'e', self.ne0)
        self.chem.set_species(self.n0, 'N2+', self.ne0)

        self.reduced_mobilities = self.chem.DEFAULT_REDUCED_MOBILITIES
        self.reduced_mobilities.update(self.reduced_mobilities)

        # All conductivities are proportional to this
        ks = co.elementary_charge / NAIR

        # This is a vector to obtain the total conductivity from densities
        self.vmu = ks * self.chem.species_vector(self.reduced_mobilities)
        
        # This is the fraction of dissipated expended in heating the gas
        if self.heating_fraction_filename:
            en, hfrac = loadtxt(self.heating_fraction_filename, unpack=True)
            self.heating_fraction = interp1d(en, hfrac)

        if self.naidis_velocity:
            self.init_naidis_velocity(120)
            self.velocity = self.naidis_velocity_eval
        else:
            self.velocity = self.parametric_velocity

        # In zf we do not include the rightmost point (ztip)
        self.ztip = self.ztip0

        
    def scratch(self):
        """ Inits the simulation from scratch. """
        # Initial time
        self.t = self.time_start
        
        # Initial mesh
        self.zf = r_[self.electrode_r: self.ztip0 - self.min_dz:
                     self.min_dz]


        # The initial densities
        self.n = self.chem.zero_densities(len(self.zf) + 1)
        self.chem.init_neutrals(self.n)
        self.chem.set_species(self.n, 'e', self.ne_initial)
        self.chem.set_species(self.n, 'N2+', self.ne_initial)

        if len(self.zf) == 0:
            # If we start directly from the electrode the configuration is
            # different
            self.zf = array([self.electrode_r])
            self.ztip = self.ztip0 + self.min_dz
            self.n = self.chem.zero_densities(len(self.zf) + 1)

            self.chem.init_neutrals(self.n)
            self.chem.set_species(self.n, 'e', self.ne0)
            self.chem.set_species(self.n, 'N2+', self.ne0)
        

        # Initial charge
        self.q = full(len(self.zf), 0.0)

        # These are really only to plot / save for t = 0
        rf = self.rise_profile(self.t)
        self.field = self.external_field(self.t, r_[self.zf, self.ztip], rf=rf) 
        self.en = self.field / NAIR / Td
        self.sigma = dot(self.vmu, self.n)
        self.v = 0.0
        self.tgas = full_like(self.en, self.tgas_initial)
        self.rstar = full_like(self.en, self.radius)
        self.epeak = 0.0
        
        # This is the coarsening level, wih 0 corresponding to finest and then
        # upwards for coarser grids.
        self.level = full_like(self.q, 0, dtype='i')

        # index0 is the starting location of the cell in terms of the finest
        # grid
        self.index0 = arange(len(self.q))

        
    def gl_init(self):
        z, wz = leggauss(self.gauss_z_degree)
        r, wr = leggauss(self.gauss_r_degree)
        theta, wtheta = leggauss(self.gauss_theta_degree)

        zn, wzn = leggauss(self.gauss_z_degree * self.gauss_neigh_factor)
        thetan, wthetan = leggauss(self.gauss_theta_degree
                                   * self.gauss_neigh_factor)

        self.tip_gauss_z_degree = (self.gauss_z_degree * self.gauss_tip_factor)
        zx, wzx = leggauss(self.tip_gauss_z_degree)
        gl.set_gl_tables(z, wz, r, wr, theta, wtheta, zn, wzn, thetan, wthetan,
                         zx, wzx)
        gl.rmax = self.radius
        gl.electrode_r = self.electrode_r
        gl.radius_growth_rate = self.radius_growth_rate
        gl.charge_layer_width = self.charge_layer_width

        
    def init_output(self):
        self.out = h5py.File(self.fname, "w-")
        self.steps = self.out.create_group('steps')
        self.parameters.h5_dump(self.out.create_group('global'))
        
        
    def run(self):
        self.init_output()
        
        i = 0
        while self.t < self.time_end:
            if (i % self.report_interval == 0):
                self.report()

            if (i > 0 and i % self.coarsening_interval == 0):
                self.refine()
                self.coarsen()

                
            # To keep the mesh uniform we sometimes use a truncated dt < self.dt
            # To estimate it we use the previous step's velocity
            trunc_dt = (self.zf[-1] + self.min_dz - self.ztip) / self.v
            
            if self.v <= 0:
                dt = self.dt
                extend = False
            elif trunc_dt < 0:
                dt = self.dt
                extend = True
            elif trunc_dt < self.dt:
                dt = trunc_dt
                extend = True
            else:
                dt = self.dt
                extend = False
                
            ztip_old = self.ztip
            v_old = self.v

            # We have to update sigma here to be able to save it in the
            # following instructions.
            self.sigma = dot(self.vmu, self.n) * self.tgas / self.initial_tgas

            if (i % self.out_interval == 0):
                self.dump(i / self.out_interval, self.SAVED_VARS_PRE)
                
            if not self.implicit_dq:
                self.step_q(dt)
            else:
                self.step_q_implicit(dt)

            self.t += dt / 2

            if (i % self.out_interval == 0):
                self.dump(i / self.out_interval, self.SAVED_VARS_MID)

            self.step_n(dt)
            self.t += dt / 2                

            if (i % self.out_interval == 0):
                self.dump(i / self.out_interval, self.SAVED_VARS_POST)

            if extend:
                self.extend_mesh()

            i += 1
            
        
    def report(self):
        """ Reports about the status of the simulation. """
        cprint(time.ctime(), 'yellow', attrs=['bold'])
        print("    t (s):          %e" % (self.t))
        print("    ztip (m):       %e" % self.ztip)
        print("    gridpoints:     %d" % len(self.q))
        print("    velocity (m/s): %e" % self.v)
        print("    epeak (V/m):    %e" % self.epeak)
        print("    potential (V):  %e" % (self.rise_profile(self.t)
                                          * self.electrode_v))

    # Variables saved at the beginning of the time step
    SAVED_VARS_PRE = ['zf', 'ztip', 'q', 'sigma', 'tgas', 'rstar', 't',
                      'level', 'index0', 'timestamp', 'species', 'epeak']

    # Variables saved at the midpoint of the time step
    SAVED_VARS_MID = []

    # Variables saved at the end of the time step
    SAVED_VARS_POST = ['en', 'field']
    
    def dump(self, step, saved_vars):
        """ Dumps data in the output file. """
        g = self.steps.require_group("%.5d" % step)
        if 'timestamp' in saved_vars:
            g.attrs['_timestamp_'] = time.time()
    
        for var in saved_vars:
            if var in {'timestamp', 'species'}:
                continue

            value = getattr(self, var)

            if type(value) == ndarray:
                g.create_dataset(var, data=value, compression='gzip')
            else:
                g.create_dataset(var, data=value)

        if 'species' in saved_vars:
            s = g.require_group("species")
            for spec in self.chem.species:
                value = self.chem.get_species(self.n, spec)
                s.create_dataset(spec, data=value, compression='gzip')

        self.out.flush()


    def load(self, fname, step):
        ifile = h5py.File(fname, "r")
        steps = ifile['steps']
        g = steps["%.5d" % step]

        for var in self.SAVED_VARS_PRE:
            if var in {'timestamp', 'species'}:
                continue

            value = array(g[var])
            if len(value.shape) == 0:
                setattr(self, var, float(value))
            else:
                setattr(self, var, value)


        s = g["species"]
        self.n = self.chem.zero_densities(len(self.zf) + 1)
        for spec in self.chem.species:
            self.chem.set_species(self.n, spec, s[spec])

        # This is to prevent an error in reporting.
        self.v = nan
        
        
    def init_naidis_velocity(self, enp, n=1000, enmax=2000):
        n0 = self.chem.zero_densities(n)
        self.chem.init_neutrals(n0)
        self.chem.set_species(n0, 'e', 1.0)

        en = linspace(enp, enmax, n)
        if self.radius_growth_rate !=0:
            r = self.radius * logspace(-1, 1, 100)
        else:
            r = array([self.radius])
        
        tgas = full_like(en, self.initial_tgas)
        
        efield = en * Td * NAIR
        dn = self.chem.fderivs(n0, en, tgas)
        k = self.chem.get_species(dn, 'e') 
        mun = self.reduced_mobilities['e']
        mu = mun / NAIR
        
        vdr = mun * en * Td
        alpha = k / vdr
        ep = enp * Td * NAIR
        
        vp = mu * ep

        def integrate(v, eh):
            y = alpha / efield**2 * vdr / (v + self.polarity * vdr)
            y = where(v + self.polarity * vdr > 0, y, 0.0)
            
            return InterpolatedUnivariateSpline(efield, y).integral(ep, eh)

        def fzero(v, eh, lf):
            vh = mu * eh
            b = log((v + self.polarity * vh) / (v + self.polarity * vp))
            a = 8
            return lf * eh * integrate(v, eh) - a - b
        
        v = empty((len(en), len(r)))

        enx = linspace(0, enmax, n)
        efieldx = enx * Td * NAIR

        for ((i, eh), (j, rj)) in itertools.product(enumerate(efieldx),
                                                    enumerate(r)):
            if eh <= ep:
                # Below Ek we might find some artificial solutions but
                # we know that there's no propagation in that case.
                v[i] = 0.0
                continue

            try:
                if self.polarity == -1:
                    vmin = mu * eh
                else:
                    vmin = 0.0
                    
                v[i, j] = bisect(fzero, vmin, 5e7,
                              args=(eh, rj * self.naidis_lf_ratio))
                
            except ValueError:
                # If no v > 0 is found to satisfy the equation, we set v=0
                v[i, j] = 0.0

        if len(r) > 1:
            self.naidis_velocity_interp = interp2d(efieldx, r, v.T)
        else:
            f = interp1d(efieldx, v[:, 0])
            # Ignore r
            self.naidis_velocity_interp = lambda e, r: f(e)
        
    def naidis_velocity_eval(self, epeak, ztip):
        r = self.rstar_tip(ztip)

        return self.naidis_extra_factor * self.naidis_velocity_interp(epeak, r)
    
    
        
    def parametric_velocity(self, epeak, ztip):
        """ The velocity of the streamer tip. 

        Parameters
        ----------
        epeak : float
           The field at the streamer's tip in V/m.
        ztip : IGNORED

        Returns
        -------
        velocity : float
           Velocity in m/s.

        """
        
        try:
            f = (1 + (self.perturbation_v_amp
                      * exp(-(self.ztip - self.perturbation_v_z)**2 /
                            self.perturbation_v_width**2)))
        except ZeroDivisionError:
            f = 1.0

        if epeak > self.epeak_min:
            return (self.head_velocity
                    + f * (epeak - self.epeak_min) * self.head_mobility)
        else:
            return 0
        

    def rstar_tip(self, ztip):
        """ Radius of the streamer head when the tip is at ztip. """
        return (ztip - self.ztip0) * self.radius_growth_rate + self.radius
    

    def step_q(self, dt):
        """ Performs an update of q, tgas and ztip (generally form t to t + dt).
        Assumes that the densities have already been evaluated 
        (at t + dt / 2).  

        The tip ztip is updated with an explicit Euler timestepping.
        This is perhaps not optimal and we could aim for a higher order
        but it makes things really simple when combined with the update of q.
        Besides, ztip evolves rather smoothly so a 1st-order integration
        is not too bad.
        """
        
        # These are values evaluated at t
        zf0 = r_[self.zf, self.ztip]
        tau0 = sqrt(self.tgas / self.initial_tgas)

        F0 = gl.fluxes(zf0, self.q, self.ztip, zf0, MAXWELL_K * self.sigma,
                       tau0 * self.rstar, tau0 * self.rstar,
                       self.rstar_tip(self.ztip))
        
        rf = self.rise_profile(self.t)

        dtgas = self.dtgas(self.t, self.q, self.ztip, tau0)

        qhalf = self.q - (0.5 * dt
                          * diff(F0 + self.sigma *
                                 self.external_fluxes(self.t, self.ztip,
                                                      zf0, rf=rf, tau=tau0)))
        tgashalf = self.tgas + 0.5 * dt * dtgas
        tauhalf = sqrt(tgashalf / self.initial_tgas)

        epeak_eps = self.rstar_tip(self.ztip) * self.tip_eps_ratio
        epeak = (self.external_field(self.t, self.ztip, rf=rf)
                 + MAXWELL_K * gl.epeak(zf0, self.q, self.ztip,
                                        self.rstar * tauhalf,
                                        self.rstar_tip(self.ztip),
                                        epeak_eps))
        v = self.velocity(epeak, self.ztip)

        # These are evaluated at t + h
        # Explicit Euler for dztip / dt:
        ztiphalf = self.ztip + 0.5 * dt * v

        zfhalf = r_[self.zf, ztiphalf]
        thalf = self.t + 0.5 * dt
        F1 = gl.fluxes(zfhalf, qhalf, ztiphalf, zfhalf, MAXWELL_K * self.sigma,
                       self.rstar * tauhalf, self.rstar * tauhalf,
                       self.rstar_tip(ztiphalf))

        dtgas = self.dtgas(thalf, qhalf, ztiphalf, tauhalf)

        rf = self.rise_profile(thalf)
        q1 = self.q - (dt
                       * diff(F1 + self.sigma *
                              self.external_fluxes(thalf, ztiphalf,
                                                   zfhalf, rf=rf, tau=tauhalf)))
        tgas1 = self.tgas + dt * dtgas
        epeak_eps = self.rstar_tip(self.ztiphalf) * self.tip_eps_ratio

        self.epeak = (self.external_field(thalf, ztiphalf, rf=rf)
                 + MAXWELL_K * gl.epeak(zfhalf, qhalf, ztiphalf,
                                        self.rstar * tauhalf,
                                        self.rstar_tip(ztiphalf),
                                        epeak_eps))
        self.v = self.velocity(self.epeak, ztiphalf)
        
        ztip1 = self.ztip + dt * self.v
        
        # This is the dilution rate interpolated to t + dt / 2
        # that will be used in step_n.
        self.dilution_rate = 0.5 * dtgas * (1 / self.tgas + 1 / tgas1)
        self.q, self.ztip, self.tgas = q1, ztip1, tgas1
            


    def step_q_implicit(self, dt):
        """ Same as above but for the update of q we use a 
        Crank-Nicolson method, which is
        relatively simple bc the system is linear but with a matrix
        that depends on time (through ztip).

        """
        
        self.sigma = dot(self.vmu, self.n)

        # These are values evaluated at t
        zf0 = r_[self.zf, self.ztip]
        tau0 = sqrt(self.tgas / self.initial_tgas)
        
        rf = self.rise_profile(self.t)
        epeak_eps = self.rstar_tip(self.ztip) * self.tip_eps_ratio

                 
        self.epeak = (self.external_field(self.t, self.ztip, rf=rf)
                      + MAXWELL_K * gl.epeak(zf0, self.q, self.ztip,
                                             self.rstar * tau0,
                                             self.rstar_tip(self.ztip),
                                             epeak_eps))
                
        self.v = self.velocity(self.epeak, self.ztip)
        delta = self.delta_factor * self.v * co.epsilon_0 / self.sigma[-1]
        
        M0 = gl.dq_matrix(zf0, self.ztip, zf0, MAXWELL_K * self.sigma,
                          self.rstar * tau0, self.rstar * tau0,
                          self.rstar_tip(self.ztip), delta).T

        A0 = dt * M0 / 2
        c0 = -dt * diff(self.sigma *
                             self.external_fluxes(self.t, self.ztip,
                                                  zf0, delta, rf=rf, tau=tau0))

        dtgas = self.dtgas(self.t, self.q, self.ztip, tau0)

        # These are evaluated at t + h
        # Explicit Euler for dztip / dt and tgas:
        ztip1 = self.ztip + dt * self.v
        zf1 = r_[self.zf, ztip1]

        tgas1 = self.tgas + dt * dtgas
        tau1 = sqrt(tgas1 / self.initial_tgas)
        
        M1 = gl.dq_matrix(zf1, ztip1, zf1, MAXWELL_K * self.sigma,
                          self.rstar * tau1, self.rstar * tau1,
                          self.rstar_tip(ztip1), delta).T
        
        A1 = dt * M1 / 2
        rf = self.rise_profile(self.t + dt)
        c1 = -dt * diff(self.sigma *
                        self.external_fluxes(self.t + dt, ztip1,
                                             zf1, delta, rf=rf, tau=tau1))
        
        q1 = self.solveq(self.q, A0, A1, c0, c1)

        # This is the dilution rate interpolated to t + dt / 2
        # that will be used in step_n.
        self.dilution_rate = 0.5 * dtgas * (1 / self.tgas + 1 / tgas1)

        self.q, self.ztip, self.tgas = q1, ztip1, tgas1

        
    def external_fluxes(self, t, ztip, zf, delta, rf=None, tau=None):
        """ Calculates all fluxes that are not due to the self-interaction
        within the channel. 
        rf (Rise Factor) is a multiplying factor for the electrode
        potential (is calculated here if None).
        """
        if rf is None:
            rf = self.rise_profile(t)

        if tau is None:
            tau = sqrt(self.tgas / self.initial_tgas)

        fluxes = gl.electrode_fluxes(rf * self.electrode_v, ztip, zf,
                                     self.rstar * tau, self.rstar_tip(ztip),
                                     delta)

        if self.uniform_field != 0:
            fluxes += gl.uniform_fluxes(self.uniform_field, ztip, zf,
                                        self.rstar * tau, self.rstar_tip(ztip),
                                        delta)


        if self.pulse_charge != 0 and t > self.pulse_start_time:
            zpulse = (t - self.pulse_start_time) * self.pulse_velocity
            fluxes += gl.pulse_fluxes(MAXWELL_K * self.pulse_charge, zpulse,
                                      self.rstar * tau, self.rstar_tip(ztip))

        return fluxes
    

    def external_field(self, t, z, rf=None):
        """ Calculates the field that is not due to self-interaction. """
        if rf is None:
            rf = self.rise_profile(t)

        field = rf * self.electrode_r * self.electrode_v / z**2

        if self.uniform_field != 0:
            field += self.uniform_field

        if ((self.pulse_charge != 0 or self.pulse_amplitude != 0)
            and t > self.pulse_start_time):
            zpulse = (t - self.pulse_start_time) * self.pulse_velocity
            field += (MAXWELL_K * self.pulse_charge
                      * ((z - zpulse)
                         / ((z - zpulse)**2 + self.pulse_distance**2)**1.5))
            field += (self.pulse_amplitude
                      * exp(-(z - zpulse)**2 / self.pulse_width**2))
    
        return field
    
            
    @staticmethod
    def dq_matrix_partial(zf0, ztip, sigma, M0, nvalid):
        """ Calculates the dq matrix but keeping the sub-matrix of M0 defined
        by the first nvalid cells . """
        M = empty_like(M0)

        # The first step is easy: copy the old data
        M[0:nvalid, 0:nvalid] = M0[0:nvalid, 0:nvalid]

        # The effect of invalid q on any flux
        M[:, nvalid:] = gl.dq_matrix(zf0[nvalid:], ztip,
                                     zf0, MAXWELL_K * sigma).T

        # The effect of valid q on invalid flux
        M[nvalid:, 0:nvalid] = gl.dq_matrix(zf0[:nvalid + 1], ztip,
                                            zf0[nvalid:],
                                            MAXWELL_K * sigma[nvalid:]).T

        return M
    
        
    @staticmethod
    def solveq(q, A0, A1, c0, c1):
        # We build the system L q(t+1) = R q(t) + c
        I = eye(len(q))
        b = q + dot(A0, q) + 0.5 * (c0 + c1)

        newq = solve(I - A1, b)

        return newq


    def dtgas(self, t, q, ztip, tau):
        if not self.heating_fraction_filename:
            return 0
        
        zf0 = r_[self.zf, ztip]
        zf0minus = r_[self.zf, self.ztip - Z_EPSILON]
        
        field = (MAXWELL_K * gl.eaxis(zf0, q, ztip, zf0minus,
                                      tau * self.rstar, self.rstar_tip(ztip))
                 + self.external_field(t, zf0minus))

        ngas = NAIR / tau**2
        
        res = ((2.0 / 7.0) * field**2 * self.sigma
               * self.heating_fraction(abs(field) / ngas / Td)
               / ngas / co.k)
        res[:] = where(zf0minus > self.zleader + Z_EPSILON,
                       res, 0.0)
        # We do not allow heating at the tip.  Usually this is anyhow
        # inconsequential but for the inception, when a streamer has not yet
        # formed we prevent an runaway heating there.
        res[-1] = 0.0
        
        return res
    
        
    def rise_profile(self, t):
        """ The rise time profile. """
        if self.rise_time == 0.0: 
            f_rise = 1.0
        else:
            f_rise = 1.0 - exp(-(t - self.rise_start) / self.rise_time)

        if self.perturbation_field_amp == 0:
            f_perturb = 1.0
        else:
            f_perturb = (1 + (self.perturbation_field_amp
                              * exp(-(t - self.perturbation_field_t)**2 /
                                    self.perturbation_field_duration**2)))

        return f_perturb * f_rise
    
    def total_potential(self, far_away=5):
        """ Calculates the potential difference between the electrode and a
        far-away point.  Only for debugging / testing. """
        zf0 = r_[self.zf, self.ztip]
        z = r_[self.zf, self.ztip, 2 * self.ztip - self.zf[::-1],
               linspace(2 * self.ztip - self.zf[0], far_away, 100)[1:]]
        
        tau = sqrt(self.tgas / self.initial_tgas)
        field = (MAXWELL_K *  gl.eaxis(zf0, self.q, self.ztip, z,
                                       self.rstar * tau)
                 + self.external_field(self.t, z))

        v = trapz(field, x=z)
        return v
    
        
    def step_n(self, dt):
        zf0 = r_[self.zf, self.ztip]
        
        # Substact a small quantity since eaxis may be discontinuous and
        # we are interested in the inner value.
        zf0minus = r_[self.zf, self.ztip + Z_EPSILON]

        tau = sqrt(self.tgas / self.initial_tgas)
        
        self.field = (MAXWELL_K * gl.eaxis(zf0, self.q, self.ztip,
                                           zf0minus, self.rstar * tau,
                                           self.rstar_tip(self.ztip))
                      + self.external_field(self.t, zf0minus))

        ngas = NAIR * self.initial_tgas / self.tgas
        self.en = abs(self.field) / ngas / Td

        c0 = (self.n + 0.5 * dt
              * (self.chem.fderivs(self.n, self.en, self.tgas)
                 - self.dilution_rate * self.n))

        # Each k is independent here: possibility of parallelization
        for k in range(len(self.zf)):
            # We neglect all changes to the left of zleader
            if self.zf[k] <= self.zleader + Z_EPSILON:
                continue
            
            err = inf
            while err > self.density_tolerance:
                # We have to include here the newaxis and squeezes here because
                # fjacobian is implemented to have z in the first axis,
                # which is the most efficient when we calculate it everywhere.
                # Perhaps there is some performance and clarity to be gained
                # by rearraging axis in fjacobian.
                # The Jacobian of the dn/dt
                j = self.chem.fjacobian(self.n[:, k, newaxis],
                                        self.en[k, newaxis],
                                        self.tgas[k, newaxis]).T

                # The jacobian of the function that we want to zero
                I = eye(self.chem.nspecies)
                fullj = (I - (0.5 * dt
                              * (squeeze(j) + 
                                 - I * self.dilution_rate[k])))

                dn = (self.chem.fderivs(self.n[:, k, newaxis],
                                        self.en[k, newaxis],
                                        self.tgas[k, newaxis])
                      - (self.dilution_rate[newaxis, k, newaxis]
                         * self.n[:, k, newaxis]))
                
                f = (self.n[:, k] - dt * squeeze(dn) / 2 - c0[:, k])

                n_plus = self.n[:, k] + solve(fullj, -f)
                err = norm(self.n[:, k] - n_plus) / norm(self.n[:, k])

                # We update n in-place
                self.n[:, k] = n_plus

        assert(amin(self.n) >= 0.0)

        
    def extend_mesh(self):
        """ Extends the mesh by one cell. """

        # We create a new cell boundary at ztip but to avoid a cell of length 0
        # we put the boundary slightly to the left, at ztip - Z_EPSILON.
        # The change in the latest cell is distributed proportionally into the
        # two cells to prevent the formation of a small hole that even if
        # infinitesimally small has a finite effect on the value of epeak.

        # This is the fraction of the (previous) last cell that goes into the
        # new cell.
        r = Z_EPSILON / (self.ztip - self.zf[-1])
        self.q = r_[self.q[:-1], (1 - r) * self.q[-1], r * self.q[-1]]

        self.zf = r_[self.zf, self.ztip - Z_EPSILON]
        
        self.level = r_[self.level, 0]
        self.index0 = r_[self.index0, self.index0[-1] + 1]

        if (self.perturbation_width > 0):
            f = (1 + (self.perturbation_amp
                      * exp(-(self.ztip - self.perturbation_z)**2 /
                            self.perturbation_width**2)))
        else:
            f = 1.0
            
        self.tgas = r_[self.tgas, self.initial_tgas] 
        self.rstar = r_[self.rstar, self.rstar_tip(self.ztip)]

        self.sigma = r_[self.sigma, f * dot(self.vmu, self.n0)]
        self.n = concatenate((self.n, f * self.n0), axis=1)

        
    def mark_coarsening(self):
        """ Marks cell boudaries that need to be merged in the
        to fullfill the smoothness criterium. """
        zf0 = r_[self.zf, self.ztip]
        dz = diff(zf0)
        eta = self.q / dz

        slope = 2 * abs(eta[1:] - eta[:-1]) / abs(eta[1:] + eta[:-1])
        newdz = dz[:-1] + dz[1:]
        
        self.flag = full_like(slope, 0)
        self.merges = 0

        for indx in argsort(newdz):
            if (slope[indx] < self.coarsening_threshold
                and (self.level[indx] == self.level[indx + 1])
                and (self.index0[indx] % 2**(self.level[indx] + 1) == 0)
                and (self.ztip - zf0[indx + 2] > self.coarsening_min_shift)
                and (dz[indx] + dz[indx + 1] <= self.coarsening_max_dz)):

                # Check for proper nesting
                proper_nesting = True
                try:
                    if abs(self.level[indx - 1] - (self.level[indx] + 1)) > 1:
                        proper_nesting = False
                except IndexError:
                    pass

                try:
                    if abs(self.level[indx + 2] - (self.level[indx] + 1)) > 1:
                        proper_nesting = False
                except IndexError:
                    pass
                
                        
                if self.flag[indx] == 0 and proper_nesting:
                    self.merges += 1
                    self.flag[indx] = 1
                    if indx > 0:
                        self.flag[indx - 1] = -1
                    if indx < len(self.flag) - 1:
                        self.flag[indx + 1] = -1

        
    def execute_coarsening(self):
        """ Executes the coarsening marked in mark_coarsening. """
        if self.merges == 0:
            return

        zf0 = r_[self.zf, self.ztip]
        q_new = empty(len(self.q) - self.merges)
        zf_new = empty(len(zf0) - self.merges)
        n_new = empty((self.n.shape[0], self.n.shape[1] - self.merges))
        tgas_new = empty(len(self.tgas) - self.merges)
        rstar_new = empty(len(self.rstar) - self.merges)
        level_new = empty(len(self.level) - self.merges)
        index0_new = empty(len(self.index0) - self.merges)
        
        i, j = 0, 0

        zf_new[0] = self.zf[0]
        n_new[:, 0] = self.n[:, 0]
        tgas_new[0] = self.tgas[0]
        rstar_new[0] = self.rstar[0]
        
        while i < len(self.q) - 1:
            if self.flag[i] == 1:
                q_prime = self.q[i] + self.q[i + 1]
                level_prime = self.level[i] + 1
                index0_prime = self.index0[i]

                zf_prime = zf0[i + 2]
                n_prime = self.n[:, i + 2]
                tgas_prime = self.tgas[i + 2]
                rstar_prime = self.rstar[i + 2]
                
                i += 1
            else:
                q_prime = self.q[i]
                level_prime = self.level[i]
                index0_prime = self.index0[i]

                zf_prime = zf0[i + 1]
                n_prime = self.n[:, i + 1]
                tgas_prime = self.tgas[i + 1]
                rstar_prime = self.rstar[i + 1]
                
            q_new[j] = q_prime
            level_new[j] = level_prime
            index0_new[j] = index0_prime
            
            zf_new[j + 1] = zf_prime
            n_new[:, j + 1] = n_prime
            tgas_new[j + 1] = tgas_prime
            rstar_new[j + 1] = rstar_prime
            
            i += 1
            j += 1

        if i == len(self.q) - 1:
            q_new[j] = self.q[i]
            level_new[j] = self.level[i]
            index0_new[j] = self.index0[i]

            zf_new[j + 1] = zf0[i + 1]
            n_new[:, j + 1] = self.n[:, i + 1]
            tgas_new[j + 1] = self.tgas[i + 1]
            rstar_new[j + 1] = self.rstar[i + 1]
            
        assert(zf0[-1] == zf_new[-1])

        self.q = q_new
        self.level = level_new
        self.index0 = index0_new
        
        self.zf = zf_new[:-1]
        self.n = n_new
        self.tgas = tgas_new
        self.rstar = rstar_new
        

    def coarsen(self):
        """ Runs the full coarsening cycle until the point where no
        more merging is possible within our coarsening_threshold.  """
        self.merges = 1
        while self.merges > 0:
            # This sets up self.merges
            self.mark_coarsening()
            self.execute_coarsening()
            

    def mark_refining(self):
        zf0 = r_[self.zf, self.ztip]
        dz = diff(zf0)
        
        eta = self.q / dz

        self.alpha = gl.fslopes(zf0, eta)
        
        # If we were to refine, the resulting this expression would be
        # the same as in mark_coarsening.
        slope = abs(self.alpha) * dz / (2 * eta)
        
        self.rflag = logical_and(slope > self.coarsening_threshold,
                                 self.level > 0)

        # Check for proper nesting
        newlevel = self.level - self.rflag
        proper = abs(diff(newlevel)) < 2
        self.rflag[1:] = logical_and(self.rflag[1:], proper)
        self.rflag[:-1] = logical_and(self.rflag[:-1], proper)
        
        self.refinings = count_nonzero(self.rflag)

        
    def execute_refining(self):
        if self.refinings == 0:
            return

        zf0 = r_[self.zf, self.ztip]
        q_new = empty(len(self.q) + self.refinings)
        level_new = empty(len(self.level) + self.refinings, 'i')
        index0_new = empty(len(self.index0) + self.refinings, 'i')

        zf_new = empty(len(self.zf) + self.refinings)
        n_new = empty((self.n.shape[0], self.n.shape[1] + self.refinings))
        tgas_new = empty(len(self.tgas) + self.refinings)
        rstar_new = empty(len(self.rstar) + self.refinings)

        dz = diff(zf0)
        eta = self.q / dz 
        
        j = 0
        for i in range(len(self.q)):
            zf_new[j] = zf0[i]
            n_new[:, j] = self.n[:, i]
            tgas_new[j] = self.tgas[i]
            rstar_new[j] = self.rstar[i]

            if self.rflag[i]:
                q_new[j] = 0.5 * self.q[i] - 0.125 * self.alpha[i] * dz[i]**2
                q_new[j + 1] = 0.5 * self.q[i] + 0.125 * self.alpha[i] * dz[i]**2
                
                level_new[j] = level_new[j + 1] = self.level[i] - 1
                index0_new[j] = self.index0[i]
                index0_new[j + 1] = self.index0[i] + 2**(self.level[i] - 1)
                
                
                zf_new[j + 1] = 0.5 * (zf0[i] + zf0[i + 1])
                n_new[:, j + 1] = 0.5 * (self.n[:, i] + self.n[:, i + 1])
                tgas_new[j + 1] = 0.5 * (self.tgas[i] + self.tgas[i + 1])
                rstar_new[j + 1] = 0.5 * (self.rstar[i] + self.rstar[i + 1])

                j += 2
            
            else:
                q_new[j] = self.q[i]
                level_new[j] = self.level[i]
                index0_new[j] = self.index0[i]
                
                j += 1


        n_new[:, -1] = self.n[:, -1]
        tgas_new[-1] = self.tgas[-1]
        rstar_new[-1] = self.rstar[-1]
        
        self.q = q_new

        self.index0 = index0_new
        self.level = level_new
        
        self.zf = zf_new
        self.n = n_new
        self.tgas = tgas_new
        self.rstar = rstar_new
        
        
    def refine(self):
        """ Runs the full refining cycle until the point where no
        more refining is needed within our refining_threshold.  """

        self.refinings = 1
        while self.refinings > 0:
            # This sets up self.refinings
            self.mark_refining()
            self.execute_refining()


    def latex(self, pdf=False):
        mobility_table = "\\\\\n".join(
            ["\\rule{0pt}{1.75em}\\ce{%s} & \\num{%.3g} & " % (spec, mun)
             for (spec, mun) in self.reduced_mobilities.items()])

        with open(self.name + '.tex', 'w') as flatex:
            flatex.write(LATEX_TEMPLATE.safe_substitute(
                name=self.name,
                parameter_table=self.parameters.latex(),
                reaction_table=self.chem.latex(),
                mobility_table=mobility_table,
                nreactions=str(self.chem.nreactions),
                nspecies=str(self.chem.nspecies)))
        if pdf:
            subprocess.call("latexmk -pdf %s" % self.name, shell=True)


class Parameters(ParamContainer):
    @param(default='')
    def description(s):
        """ A human-readable description of the code. """
        return s

    @param(default=False)
    def implicit_dq(s):
        """ Whether dq/dt is solved implicitly. """
        return s

    @param()
    def electrode_r(s):
        """ The radius of the electrode. """
        return float(s)

    @param()
    def electrode_v(s):
        """ The voltage of the electrode. """
        return float(s)
    
    @param(default=0.0)
    def uniform_field(s):
        """ External applied uniform field. """
        return float(s)
    
    @param(default=0.0)
    def rise_time(s):
        """ The rise time of the potential. """
        return float(s)

    @param(default=0.0)
    def rise_start(s):
        """ The start of the potential rise. """
        return float(s)
    
    @param(default=0.0)
    def time_start(s):
        """ The rise time of the potential. """
        return float(s)
    
    @param()
    def time_end(s):
        """ The final time. """
        return float(s)

    @param()
    def radius(s):
        """ The radius of the channel. """
        return float(s)

    @param(default=0.0)
    def radius_growth_rate(s):
        """ The spatial growth rate of the streamer radius. """
        return float(s)

    @param(default=0.0)
    def charge_layer_width(s):
        """ DEPRECATED. """
        return float(s)

    @param(default=1.0)
    def delta_factor(s):
        """ Additional factor in the width of the current layer. """
        return float(s)

    @param(default=-1)
    def polarity(s):
        """ The polarity of the streamer. """
        return int(s)
    
    @param()
    def ne0(s):
        """ The boundary condition for electrons at the head."""
        return float(s)

    @param()
    def ne_initial(s):
        """ The initial electron density. """
        return float(s)

    @param(default=300.0)
    def tgas_initial(s):
        """ The initial gas temperature in the pre-leader. """
        return float(s)

    @param(default=0.0)
    def epeak_min(s):
        """ Minimum field for streamer progression. """
        return float(s)

    @param(default=0.0)
    def head_mobility(s):
        """ Mobility of the tip after substracting epeak_min. """
        return float(s)

    @param(default=0.0)
    def head_velocity(s):
        """ Fixed additional velocity for the head. """
        return float(s)

    @param(default=False)
    def naidis_velocity(s):
        """ Use Naidis' expression for the velocity. """
        return s

    @param(default=0.5)
    def naidis_lf_ratio(s):
        """ In Naidis expression, value of lf / r = 2 lf / d = 2 / xi. """
        return float(s)
    
    @param(default=1.0)
    def naidis_extra_factor(s):
        """ Additional factor to test the role of velocity. """
        return float(s)

    @param(default=1e-3)
    def tip_eps_ratio(s):
        """ Ratio of epsilon in epeak to dz / Gauss-Legendre-order. """
        return float(s)
    
    @param()
    def min_dz(s):
        """ The min dz controlling the addition of new gridpoints. """
        return float(s)

    @param()
    def coarsening_threshold(s):
        """ The threshold for coarsening. """
        return float(s)

    @param(default=1e20)
    def refining_threshold(s):
        """ The threshold for refining. """
        return float(s)
    
    @param()
    def coarsening_interval(s):
        """ We run the coarsening scheme every coarsening_interval steps. """
        return int(s)

    @param(default=1)
    def coarsening_max_dz(s):
        """ Maximum dz in the coarsening scheme. """
        return float(s)

    @param(default=0.0)
    def coarsening_min_shift(s):
        """  Minimum separation from the tip to apply coarsening."""
        return float(s)

    @param(default=0)
    def refining_min_dz(s):
        """ Minimum dz in the refining scheme. """
        return float(s)

    @param(default=1e-7)
    def extend_tip_dz(s):
        """ Minimum dz in the refining scheme. """
        return float(s)    
    
    @param()
    def dt(s):
        """ The time step. """
        return float(s)

    @param()
    def ztip0(s):
        """ The initial tip position. """
        return float(s)

    @param(default=dict())
    def reduced_mobilities(d):
        """ Dictionary with reduced mobilities of mobile species. """
        return {k:float(v) for k, v in d.items()}

    @param(default=0.0)
    def perturbation_z(s):
        """ Location of a perturbation in the boundary densities. """
        return float(s)

    @param(default=0.0)
    def perturbation_amp(s):
        """ Amplitude of a perturbation in the boundary densities. """
        return float(s)

    @param(default=0.0)
    def perturbation_width(s):
        """ Width (e-folding) of a perturbation in the boundary densities. """
        return float(s)    

    @param(default=0.0)
    def perturbation_v_z(s):
        """ Location of a perturbation in the velocity. """
        return float(s)

    @param(default=0.0)
    def perturbation_v_amp(s):
        """ Amplitude of a perturbation in the velocity. """
        return float(s)

    @param(default=0.0)
    def perturbation_v_width(s):
        """ Width (e-folding) of a perturbation in the velocity. """
        return float(s)    

    @param(default=0.0)
    def perturbation_field_t(s):
        """ Time of a perturbation in the imposed field. """
        return float(s)

    @param(default=0.0)
    def perturbation_field_amp(s):
        """ Amplitude of a perturbation in the imposed field. """
        return float(s)

    @param(default=0.0)
    def perturbation_field_duration(s):
        """ Duration (e-folding) of a perturbation in the imposed field. """
        return float(s)    
    
    @param(default=0.0)
    def pulse_charge(s):
        """ Charge transported by an adjacent streamer head. """
        return float(s)    

    @param(default=0.0)
    def pulse_velocity(s):
        """ Velocity of an adjacent streamer head. """
        return float(s)    

    @param(default=0.0)
    def pulse_distance(s):
        """ Lateral distance of an adjacent streamer head. """
        return float(s)    

    @param(default=nan)
    def pulse_start_time(s):
        """ Start time of an adjacent streamer head. """
        return float(s)

    @param(default=0.0)
    def pulse_amplitude(s):
        """ Amplitude of an additional / alternative pulse. """
        return float(s)

    @param(default=0.0)
    def pulse_width(s):
        """ Width of an additional / alternative pulse. """
        return float(s)    
    
    @param()
    def density_tolerance(s):
        """ The relative tolerance for the evolution of densities. """
        return float(s)

    @param(default=16)
    def gauss_z_degree(s):
        """ Degree of the Gauss integration along z. """
        return int(s)

    @param(default=16)
    def gauss_theta_degree(s):
        """ Degree of the Gauss integration along theta. """
        return int(s)

    @param(default=6)
    def gauss_r_degree(s):
        """ Degree of the Gauss integration along r (ignored). """
        return int(s)

    @param(default=8)
    def gauss_neigh_factor(s):
        """ Degree increase of the Gauss integration for neighboring cells. """
        return int(s)

    @param(default=2000)
    def gauss_tip_factor(s):
        """ Degree increase of the Gauss integration for tip fields.. """
        return int(s)
    
    @param(default=1e-4)
    def dq_matrix_cache_factor(s):
        """ Factor relative to r where we recompute the M matrix. """
        return float(s)
        
    @param(default=10)
    def out_interval(s):
        """ Number of steps between data dumps. """
        return int(s)

    @param(default=10)
    def report_interval(s):
        """ Number of steps between status reports. """
        return int(s)
    
    @param(default="chemistry.default.DryAir")
    def chemistry(s):
        """ Chemistry module. """
        return s

    @param(default=dict())
    def chemistry_args(d):
        """ Dictionary with extra arguments for the chemistry module. """
        return {k:float(v) for k, v in d.items()}

    @param(default="swarm/heating_fraction.dat")
    def heating_fraction_filename(s):
        """ File with E/n (Td) ~ Fraction of energy expended in fast heating. """
        return s

    @param(default=300.0)
    def initial_tgas(s):
        """ Initial temperature. """
        return float(s)

    @param(default=0.0)
    def zleader(s):
        """ Location of the leader: densities are not updated to the left. """
        return float(s)
    

    
def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="The input file (.yaml/.json/.h5)")
    parser.add_argument("--inspect", "-i",
                        help="Run an inspecting command",
                        choices=["latex", "chemistry", "velocity"],
                        default=None)

    parser.add_argument("--load", "-l", action="store", default=None,
                        help="Load a previous step specified as file.h5:step")

    args = parser.parse_args()

    params = Parameters()
    params.file_load(args.input)
    params.report()

    rid = os.path.splitext(args.input)[0]
    sausage = Sausage(rid, params)

    if args.inspect:
        inspect(sausage, args.inspect)
        sys.exit(0)

    if args.load:
        fname, step = args.load.split(':')
        step = int(step)
        sausage.load(fname, step)
    else:
        sausage.scratch()
    
    sausage.run()

    
def inspect(sausage, cmd):
    def latex():
        sausage.latex(pdf=True)

    def chemistry():
        cprint("Running a suite of constant-field chemistry cases",
               'red', attrs=['bold'])
        from chemistry.analyze import const_field_run
        const_field_run(sausage)

    def velocity():
        en = linspace(0, 999, 1000)
        efield = en * NAIR * Td
        v = empty_like(efield)
        
        for i, iefield in enumerate(efield):
            v[i] = sausage.velocity(iefield, sausage.ztip0)

        savetxt(sausage.name + '_velocity.dat', c_[efield, en, v])
        
        
    d = {'latex': latex, 'chemistry': chemistry, 'velocity': velocity}

    d[cmd]()
    
    
LATEX_TEMPLATE = string.Template(
r"""
% This latex has been produced automatically.
\documentclass{article}

%\usepackage{amsmath}
\usepackage{siunitx}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[varg]{txfonts}
\usepackage{graphicx}
\usepackage{empheq}
\usepackage[version=3]{mhchem}
\usepackage{pdflscape}
\usepackage{longtable}
\usepackage[table]{xcolor}
\usepackage{geometry}
\usepackage{cprotect}
\geometry{a4paper, landscape, margin=1cm}

\date{\today}
\cprotect\title{\verb|${name}|}

\begin{document}
\pagestyle{empty}
\maketitle
\section{Input parameters}

  \rowcolors{2}{blue!7}{white}
  \begin{longtable}{llp{12cm}}
    \rowcolor{blue!20}
    \rule{0pt}{2em} \textbf{Parameter} & \textbf{Value} & \textbf{Explanation} \\
    ${parameter_table}
  \end{longtable}

\newpage
\section{Chemical model}
\subsection{Reactions}
  \rowcolors{2}{blue!7}{white}
  \begin{longtable}{p{5em}lllp{6cm}}
    \rowcolor{blue!20}
    \rule{0pt}{2em} & \textbf{Reaction} & & \textbf{Rate (\si{m^{3(n-1)}s^{-1}})} & \textbf{Reference} \\
    ${reaction_table}
  \end{longtable}

${nreactions} reactions, ${nspecies} species\\
\subsection{Mobilities}
  \rowcolors{2}{blue!7}{white}
  \begin{longtable}{llp{6cm}}
    \rowcolor{blue!20}
    \textbf{Species} & \textbf{Reduced mobility (\si{m^{-1}V^{-1}s^{-1}})} & \textbf{Reference} \\
    ${mobility_table}
  \end{longtable}
    
\newcommand{\jcp}{J. Chem. Phys. } 
\newcommand{\ssr}{Space Sci. Rev.} 
\newcommand{\planss}{Plan. Spac. Sci.} 
\newcommand{\pre}{Phys. Rev. E} 
\newcommand{\nat}{Nature} 
\newcommand{\icarus}{Icarus} 
\newcommand{\ndash}{-} 
\newcommand{\plusmn}{\ensuremath{\pm}}
\bibliographystyle{apalike}

% Change this if you want to use another bibliography name.
\bibliography{library}

\end{document}
""")

if __name__ == '__main__':
    main()
