#! /usr/bin/env python3.4

from numpy import *
import scipy.constants as co
from scipy.interpolate import interp1d
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
import h5py

from list import select_steps

Td = 1e-17 * co.centi**2
NAIR = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')

mpl.rcParams["axes.formatter.limits"] = [-4, 4]
def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", help="Input file")

    parser.add_argument("var", help="The variable to plot")

    parser.add_argument("steps", nargs='?',
                        help="Step/steps to plot (empty for all)",
                        default='all')

    parser.add_argument("--mode", default="lines",
                        choices=['lines', 'map'],
                        help="Mode of plot")

    parser.add_argument("--cmap", default="rainbow",
                        help="Colormap to use")

    parser.add_argument("--ofile", "-o", default=None,
                        help="Output to this file.")

    
    parser.add_argument("--xlim", help="Limits of the z scale (z0:z1)", 
                        action='store', default=None)

    parser.add_argument("--ylim", help="Limits of the z scale (z0:z1)", 
                        action='store', default=None)
    
    parser.add_argument("--clim", help="Limits of the color scale (c0:c1)", 
                        action='store', default=None)

    parser.add_argument("--dots", action='store_true',
                        help="Add dots at line boundaries")
    parser.add_argument("--logx", action='store_true',
                        help="Logarithmic scale on X")
    parser.add_argument("--logy", action='store_true',
                        help="Logarithmic scale on Y")
    parser.add_argument("--logc", action='store_true',
                        help="Logarithmic scale on colorbar")
    parser.add_argument("--background",
                              default=False, action='store_true',
                              help="Plot the background field")

    args = parser.parse_args()

    modes = {'lines': LinePlot, 'map': MapPlot}
    plot = modes[args.mode](args)

    plot.init()
    plot.make()
    plot.finish()
    if args.ofile is None:
        plt.show()
    else:
        plt.savefig(args.ofile)
    
    
class Plot(object):
    def __init__(self, args):
        self.args = args
        self.ifile = h5py.File(args.ifile, "r")

        self.steps_grp = self.ifile["steps"] 
        allsteps = list(self.steps_grp.keys())

        self.steps = select_steps(args.steps, allsteps)

        varfull = args.var.split(':')

        self.var = VARIABLES[varfull[0]]
        self.rest = varfull[1:]

        self.start = array(self.steps_grp[self.steps[0]]['t'])
        self.end   = array(self.steps_grp[self.steps[-1]]['t'])

        if self.end == 0:
            # Allow plotting even when we have a single step
            self.end = 1

        self.t = zeros(len(self.steps))

        self.fig = plt.figure(args.ifile)
        self.ax = plt.axes([0.15, 0.10, 0.65, 0.8])
        if len(self.steps) > 1:
            self.cax = plt.axes([0.85, 0.10, 0.05, 0.8])


class LinePlot(Plot):
    def init(self):
        self.cmap = plt.get_cmap(self.args.cmap)

        
    def make(self):
        for i, step in enumerate(self.steps):
            g = self.steps_grp[step]
            self.t[i] = array(g['t'])
            if len(self.steps) > 1:
                tfrac = float((self.t[i] - self.start) / (self.end - self.start))
                color = self.cmap(tfrac)
            else:
                color = 'r'
                self.ax.text(0.0, 1.0, 't = %d ns' % int(self.t[i] / co.nano),
                             ha='left',
                             va='bottom',
                             size='large',
                             color='#000055',
                             transform=self.ax.transAxes)

            x, y = self.var(g, *self.rest)

            style = '-' if not self.args.dots else 'o-'
            self.ax.plot(x, y, style, lw=1.8, c=color, alpha=0.8,
                         ms=1.5, mfc='k')

            
    def finish(self):
        if len(self.steps) > 1:
            norm = mpl.colors.Normalize(vmin=self.t[0], vmax=self.t[-1])
            cbar = mpl.colorbar.ColorbarBase(self.cax,
                                             cmap=self.cmap,
                                             norm=norm,
                                             orientation='vertical')

            cbar.set_label(r'Time ($\mathdefault{s}$)')

        if self.args.logx:
            self.ax.set_xscale('log')

        if self.args.logy:
            self.ax.set_yscale('log')

        electrode_r = self.ifile["global"].attrs["electrode_r"]
        self.ax.axvline(electrode_r, lw=2.0, c='k')
    
        self.ax.grid(True)
        if not self.rest:
            self.ax.set_ylabel("%s ($\\mathdefault{%s}$)"
                               % (self.var.name, self.var.units))
        else:
            self.ax.set_ylabel("%s of %s ($\\mathdefault{%s}$)"
                               % (self.var.name, self.rest[0], self.var.units))
            
        self.ax.set_xlabel("z (m)")

        if self.args.xlim:
            xlim = [float(x) for x in self.args.xlim.split(':')]
            self.ax.set_xlim(xlim)
            
        if self.args.ylim:
            ylim = [float(x) for x in self.args.ylim.split(':')]
            self.ax.set_ylim(ylim)

        if self.args.background:
            electrode_v = self.ifile["global"].attrs["electrode_v"]
        
            ax.plot(x, electrode_r * electrode_v / x**2,
                    lw=2.0, c='#777777')


class MapPlot(Plot):
    def init(self):
        n = 500
        self.cmap = plt.get_cmap(self.args.cmap)
        # Find the longest channel
        g = self.steps_grp[self.steps[-1]]
        x, _ = self.var(g, *self.rest)
        self.x = linspace(x[0], x[-1], n)
        self.m = empty((len(self.t), len(self.x)))


    def make(self):
        for i, step in enumerate(self.steps):
            g = self.steps_grp[step]
            self.t[i] = array(g['t'])
            x, y = self.var(g, *self.rest)
            self.m[i, :] = interp1d(x, y, bounds_error=False,
                                    fill_value=nan)(self.x)
        norm = None if not self.args.logc else LogNorm()
        

        kwargs = {}
        if self.args.clim is not None:
            clim = [float(x) for x in self.args.clim.split(':')]
            kwargs['vmin'] = clim[0]
            kwargs['vmax'] = clim[1]
            
        map = self.ax.pcolormesh(self.t, self.x,
                                 ma.masked_where(isnan(self.m.T), self.m.T),
                                 norm=norm, cmap=self.cmap, **kwargs)
        self.cbar = plt.colorbar(map, cax=self.cax)
        
    def finish(self):
        if self.args.logx:
            self.ax.set_xscale('log')

        if self.args.logy:
            self.ax.set_yscale('log')

        electrode_r = self.ifile["global"].attrs["electrode_r"]
        self.ax.axhline(electrode_r, lw=2.0, c='k')
        self.cbar.set_label("%s ($\\mathdefault{%s}$)"
                            % (self.var.name, self.var.units))
        self.ax.invert_yaxis()
        self.ax.set_xlim([self.t[1], self.t[-1]])
        self.ax.set_ylabel("z (m)")
        self.ax.set_xlabel("t (s)")

        
class Mesh(object):
    def __init__(self, step_grp):
        self.zf = array(step_grp['zf'])
        self.ztip = float(array(step_grp['ztip']))
        self.zf0 = r_[self.zf, self.ztip] 
        self.zc = 0.5 * (self.zf0[1:] + self.zf0[:-1])
        self.dz = diff(self.zf0)

        
class Variable(object):
    def __init__(self, func, doc=None, name="", units="?"):
        self.func = func
        self.name = name
        self.units = units
        self.doc = doc

    def __call__(self, step_grp, *args):
        mesh = Mesh(step_grp)
        r = self.func(step_grp, mesh, *args)
        return r
        

VARIABLES = {}
def variable(**kwargs):
    """ A decorator to define variables from functions. """
    def deco(f):
        kwargs.update({'doc': f.__doc__})
        VARIABLES[f.__name__] = Variable(f, **kwargs)
        return f
    return deco


@variable(name="Charge density", units="C/m")
def charge(g, mesh):
    q  = array(g["q"])
    return mesh.zc, q / mesh.dz

@variable(name="Charge density", units="C")
def cell_charge(g, mesh):
    q  = array(g["q"])
    return mesh.zc, q

@variable(name="Charge volume density", units="e/m^3")
def volcharge(g, mesh):
    q  = array(g["q"])
    radius = g.file["global"].attrs["radius"]
    return mesh.zc, q / mesh.dz / (pi * radius**2) / co.elementary_charge


@variable(name="Transversal field", units="V/m")
def etrans(g, mesh):
    q  = array(g["q"])
    field  = array(g["field"])
    radius = g.file["global"].attrs["radius"]

    eta = q / mesh.dz
    dfield = diff(field) / mesh.dz
    a = 1 / (2 * pi * co.epsilon_0 * radius)
    return mesh.zc, a * (eta - pi * radius**2 * co.epsilon_0 * dfield)



@variable(name="Cumulative charge", units="C")
def cumcharge(g, mesh):
    q  = array(g["q"])
    return mesh.zf0, r_[0.0, cumsum(q)]

@variable(name="Reduced field ($E/n$)", units="Td")
def en(g, mesh):
    en  = array(g["en"])    
    return mesh.zf0, en

@variable(name="Electric field", units="V/m")
def field(g, mesh):
    field  = array(g["field"])
    try:
        field[-1] = array(g["epeak"])
    except KeyError:
        pass
    
    return mesh.zf0, field

@variable(name="Conductivity", units="1 / \Omega m^2")
def sigma(g, mesh):
    sigma  = array(g["sigma"])

    return mesh.zf0, sigma

@variable(name="Gas temperature", units="K")
def tgas(g, mesh):
    sigma  = array(g["tgas"])
    return mesh.zf0, sigma

@variable(name="Radius", units="m")
def rstar(g, mesh):
    sigma  = array(g["rstar"])
    return mesh.zf0, sigma

@variable(name="Current density", units="A / m^2")
def current(g, mesh):
    sigma  = array(g["sigma"])
    field  = array(g["field"])
    return mesh.zf0, sigma * field

@variable(name="Dissipated power", units="W / m^3")
def power(g, mesh):
    sigma  = array(g["sigma"])
    field = array(g["field"])
    return mesh.zf0, sigma * field**2

@variable(name="Heating rate", units="K / s")
def heating(g, mesh):
    sigma  = array(g["sigma"])
    field = array(g["field"])
    return mesh.zf0, 2 * sigma * field**2 / (5 * co.k * NAIR)

@variable(name="Maxwell relaxation time", units="s")
def maxwell_time(g, mesh):
    sigma  = array(g["sigma"])
    return mesh.zf0, co.epsilon_0 / sigma

@variable(name="Density", units="m^{-3}")
def species(g, mesh, spec):
    s = g['species']
    d = s[spec]
    
    return mesh.zf0, d

@variable(name="Carrier density", units="m^{-3}")
def carriers(g, mesh):
    s = g['species']
    a = array([dens for name, dens in s.items()])
    
    d = sum(a, axis=0)
    
    return mesh.zf0, d

@variable(name="Cells", units="")
def cells(g, mesh):
    return mesh.zf0, full_like(mesh.zf0, g["t"])

@variable(name="Widths", units="m")
def widths(g, mesh):
    return mesh.zc, mesh.dz


if __name__ == '__main__':
    main()
