""" This is a library to parse, verify and use parameters mostly
designed for numerical codes. """
import sys
import os
import socket
import warnings
import time
import pickle
import pwd

import numpy as np
from termcolor import colored, cprint

class ParameterNotAllowed(ValueError):
    pass


class Parameter(object):
    """ This is a class containing the description, conversion/verification 
    routines. """
    def __init__(self, name, func=None, doc='', verifiers=None, default=None):
        self.name = name
        self.func = func
        self.doc = doc
        if verifiers is None:
            verifiers = []

        self.verifiers = verifiers
        self.default = default


    def __call__(self, s):
        return self.func(s)


    def __set__(self, obj, value):
        for vf in self.verifiers:
            vf(value)
        obj.values[self.name] = value


    def __get__(self, obj, objtype):
        try:
            return obj.values[self.name]
        except KeyError as e:
            if self.default is None:
                raise
            else:
                return self.default

            
def isspecial(name):
    """  Determines if a parameter name is 'special' and has to be handled
    differently.  Special parameters start and end with an underscore
    (maybe two or more, but this is disencouraged). """
    return (name[0] == '_' and name[-1] == '_')


class ParamContainer(object):
    """ You must sub-class this to implement your own verifiers. """
    def __init__(self):
        self.values = {}

        self.params = {k: v for k, v in type(self).__dict__.items() 
                       if isinstance(v, Parameter)}
        self.param_names = {k for k, v in self.params.items()}
        
        

    def dict_load(self, d, warn_undef=True):
        """ Load the parameters from a dictionary-like object.  All other
        loaders must be based on this one. """
        for key, value in d.items():
            if key.startswith('blob_'):
                key = key[len('blob_'):]
                value = pickle.loads(value)
                
            if key in self.param_names:
                try:
                    p = self.params[key]
                    if p.func is not None:
                        value = p.func(value)

                    setattr(self, key, value)
                except Exception as e:
                    # Note that we do not want to preserve the traceback
                    
                    raise type(e)('Setting "{}" to {}: {}'
                                  .format(key, str(value), str(e)))
            else:
                if not isspecial(key) and warn_undef:
                    warnings.warn("Ignoring undefined parameter '{}'"
                                  .format(key))

    
    

    def asdict(self):
        return {name: getattr(self, name) 
                for name, p in self.params.items()}

    def metadict(self):
        """ Returns a dictionary extended with metadata.  This is
        the default for dumping. """
        d = {'_timestamp_': time.time(),
             '_ctime_': time.ctime(),
             '_command_': ' '.join(sys.argv),
             '_cwd_': os.getcwd(),
             '_user_': pwd.getpwuid(os.getuid())[0],
             '_host_': socket.gethostname()}

        d.update(self.asdict())
        return d


    def export(self, other):
        """ Populates other with the parameter values. """
        for name, p in self.params.items():
            setattr(other, name, getattr(self, name))
            
        
    def report(self):
        for name in sorted(self.params.keys()):
            p = self.params[name]
            desc = colored("# %s" % p.doc, 'blue')
            pname = colored("%-28s" % name, 'yellow', attrs=['bold'])
            pvalue = "%-25s" % repr(getattr(self, name))
            cprint("%s = %s    %s" % (pname, pvalue, desc))

    ####
    # Now we implement the loaders / dumpers for the supported formats.
    
    def file_load(self, fname):
        """ Loads a file, deciding its format from the extension. """
        loaders = {'.yaml': self.yaml_loadf,
                   '.json': self.json_loadf,
                   '.h5': self.h5_loadf}

        loader = loaders[os.path.splitext(fname)[1]]
        loader(fname)


    def file_dump(self, fname):
        """ Dumps into a file, deciding its format from the extension. """
        dumpers = {'.yaml': self.yaml_dumpf,
                   '.json': self.json_dumpf,
                   '.h5': self.h5_dumpf}

        dumper = dumpers[os.path.splitext(fname)[1]]
        dumper(fname)

    
    ### YAML loaders / dumpers

    def yaml_loadf(self, fname):
        with open(fname) as fp:
            self.yaml_load(fp)


    def yaml_load(self, fp):
        import yaml

        d = yaml.load(fp)
        self.dict_load(d)


    def yaml_dumpf(self, fname):
        with open(fname, 'w') as fp:
            self.yaml_dump(fp)


    def yaml_dump(self, fp):
        import yaml
        
        yaml.dump(self.metadict(), fp, width=50, indent=4, 
                  default_flow_style=False)


    def yaml_dumps(self):
        import yaml

        yaml.dump(self.metadict())



    ### JSON loaders / dumpers

    def json_loadf(self, fname):
        with open(fname) as fp:
            self.json_load(fp)


    def json_load(self, fp):
        """ Load parameters from a json file. """
        import json

        raw = json.load(fp)
        self.dict_load(raw)


    def json_loads(self, s):
        """ Load parameters from a json-formatted string. """
        import json

        raw = json.loads(s)
        self.dict_load(raw)


    def json_dumpf(self, fname):
        with open(fname, 'w') as fp:
            self.json_dump(fp)


    def json_dump(self, fp):
        import json

        return json.dump(self.metadict(), fp, indent=4)


    def json_dumps(self):
        import json

        return json.dumps(self.metadict())


    ### HDF5 loaders/dumpers
    def h5_loadf(self, fname):
        import h5py

        fp = h5py.File(fname, 'r')
        self.h5_load(fp)
        fp.close()


    def h5_load(self, group):
        """ Load the parameters from an hdf5 file. """
        self.dict_load(group.attrs)


    def h5_dumpf(self, fname):
        import h5py

        fp = h5py.File(fname)
        self.h5_dump(fp)
        fp.close()


    def h5_dump(self, group):
        for k, v in self.metadict().items():
            try:
                group.attrs[k] = v
            except TypeError:
                group.attrs['blob_' + k] = np.void(pickle.dumps(v))

    def rst_dump(self):
        """ Converts the input parameter descriptors into an .rst doc file. """
        def bracket(s):
            return '[%s]' % str(s) if s else ''

        return "\n\n\n".join("``%s``\n\n   %s %s" 
                             % (p.name, p.doc, bracket(p.default)) 
                             for k, p in self.params.items())

    def latex(self):
        rows = []
        def cleanword(word):
            if word in self.params.keys() or '_' in word:
                return r"\verb|%s|" % word
            else:
                return word
        
        for name in sorted(self.params.keys()):
            p = self.params[name]
            value = getattr(self, name)
            doc = " ".join(cleanword(word) for word in p.doc.split())
            try:
                if np.isnan(value):
                    value = "NaN"
                else:
                    value = r"\num{%s}" % float(value)
            except (ValueError, TypeError):
                value = r"\verb|%s|" % repr(value)
            
            row = (r"\rule{0pt}{2em} \verb|%s| & %s & %s \\"
                   % (p.name, value, doc))
            rows.append(row)

        return "\n".join(rows)
            

def param(*args, **kwargs):
    default = kwargs.get('default', None)
    def deco(func):
        return Parameter(func.__code__.co_name, func, doc=func.__doc__, 
                         verifiers=args, default=default)
    return deco

                     
# Here we define some decorators for the most common parameter values.
def positive(s):
    if s <= 0:
        raise ParameterNotAllowed("Parameter must be positive")
    return s


def nonnegative(s):
    """ Use this decorator to check that a parameter is not negative. """
    if s <= 0:
        raise ParameterNotAllowed("Parameter must be positive")
    return s



def contained_in(l):
    """ Use this decorator to check that a parameter is contained in 
    a given set/list. """    
    def _contained(s):
        if s not in l:
            raise ParameterNotAllowed("Parameter must be one of {{{}}}"
                                          .format(','.join(str(s) for s in l)))
        return s
    return _contained
